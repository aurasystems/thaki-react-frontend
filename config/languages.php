<?php

return [
    'en' => [
        'display' => 'English',
        'flag-icon' => 'us'
    ],
    'ar' => [
        'display' => 'عربي',
        'flag-icon' => 'ar'
    ],
];
