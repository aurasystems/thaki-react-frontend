<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Frontend Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used during authentication for various
      | messages that we need to display to the user. You are free to modify
      | these language lines according to your application's requirements.
      |
     */

    'app_name' => 'Thaki',
    'lang_name' => 'English',
    'lang_code' => 'en',
    'lang_dir' => 'ltr',
    'currency_label' => 'SAR',
    'copyright_text' => 'Copyright',
    'error_label' => 'Error',
    'private_area_access_msg' => 'You need to login first!',
    // Months
    'month_1' => 'January',
    'month_2' => 'February',
    'month_3' => 'March',
    'month_4' => 'April',
    'month_5' => 'May',
    'month_6' => 'June',
    'month_7' => 'July',
    'month_8' => 'August',
    'month_9' => 'September',
    'month_10' => 'October',
    'month_11' => 'November',
    'month_12' => 'December',
    // Days
    'day_plural' => 'Day/s',
    'hour_plural' => 'H',
    'hour_s_label' => 'Hour/s',
    // Form fields
    'name_field' => 'Name',
    'email_field' => 'Email',
    'phone_field' => 'Phone',
    'age_field' => 'Age',
    'birth_date_field' => 'Birth Date',
    'old_password_field' => 'Old Password',
    'password_field' => 'Password',
    'password_confirmation_field' => 'Confirm Password',
    'id_card_front_img_field' => 'Personal identity front image',
    'id_card_back_img_field' => 'Personal identity back image',
    // General buttons
    'submit_request_button' => 'Submit request',
    'confirm_button' => 'Confirm',
    'cancel_button' => 'Cancel',
    'set_default_button' => 'Set default',
    'select_button' => 'Select',
    'edit_button' => 'Edit',
    'delete_button' => 'Delete',
    'back_button' => 'Back',
    'save_button' => 'Save',
    'close_button' => 'Close',
    // General Messages
    'confirm_action_msg' => 'Confirm action',
    'confirm_delete_msg' => 'Are you sure you want to delete?',
    'confirm_default_msg' => 'Are you sure you want to set default?',
    'confirm_buy_package_msg' => 'Are you sure you want to buy this package?',
    'confirm_buy_subscription_msg' => 'Are you sure you want to buy this subscription?',
    'confirm_cancel_booking_msg' => 'Are you sure you want to cancel booking?',
    'saved_success_msg' => 'Saved successfully',
    'no_bookings_found' => 'No bookings found',
    'no_cars_found_add_one' => 'No cars found, Please add one.',
    'no_cards_found_add_one' => 'No cards found, Please add one.',
    // Login Page
    'login_label' => 'Login',
    'logout_label' => 'Logout',
    'login_desc' => 'Login to your account',
    'login_button' => 'Login',
    'login_proccess' => 'Logging You In...',
    'login_success_redirect' => 'Login successful, redirecting...',
    // Register Page
    'register_label' => 'Register',
    'register_desc' => 'Create your account',
    'register_button' => 'Create your account',
    'register_success_redirect' => 'Registration successful, redirecting...',
    // Profile Page
    'profile_label' => 'My Profile',
    'profile_button' => 'My Profile',
    // Dashboard Page
    'dashboard_label' => 'Dashboard',
    'my_balance_label' => 'My Balance',
    'my_bookings_label' => 'My Bookings',
    'current_balance_label' => 'Current Balance',
    'remaining_label' => 'Remaining',
    'valid_from_label' => 'Valid from',
    'valid_till_label' => 'Valid till',
    'balance_detailes_label' => 'Balance Detailes',
    'from_label' => 'From',
    'to_label' => 'To',
    'park_for' => 'Park for',
    'book_parking_button' => 'Book your Parking',
    'recharge_balance_button' => 'Recharge your Balance',
    'apply_subscription_button' => 'Apply for Subscription',
    'buy_subscription_button' => 'Buy Subscription',
    'select_car_label' => 'Select Car',
    'immediate_parking_button' => 'Immediate Parking',
    'immediate_parking_desc' => 'Park Now',
    'schedule_parking_button' => 'Schedule Parking',
    'schedule_parking_desc' => 'Schedule your parking',
    // My Parkings Page
    'my_parkings_label' => 'My Parkings',
    'upcoming_label' => 'Upcoming',
    'pending_label' => 'Pending',
    'completed_label' => 'Completed',
    'cancelled_label' => 'Cancelled',
    'qr_button' => 'View QR',
    // My Cards Page
    'cards_label' => 'My Cards',
    'card_holder_field' => 'Card holder',
    'card_number_field' => 'Card number',
    'card_expiry_field' => 'Card expiry',
    'card_cvv_field' => 'Card CVV',
    'card_default_field' => 'Default',
    'card_default_label' => 'Default',
    'add_card_button' => 'Add Card',
    'add_card_label' => 'Add card',
    // Cars Page
    'cars_label' => 'My Cars',
    'edit_car_label' => 'Edit car',
    'car_name_field' => 'Car name',
    'plate_number_en_field' => 'Plate number in english',
    'plate_number_ar_field' => 'Plate number in arabic',
    'state_field' => 'State',
    'make_field' => 'Make',
    'model_field' => 'Model',
    'color_field' => 'Color',
    'year_field' => 'Year',
    'car_default_field' => 'Favorite',
    'car_default_label' => 'Favorite',
    'add_car_button' => 'Add Car',
    'add_car_label' => 'Add car',
    // Buy package Page
    'buy_package_label' => 'Recharge my balance',
    'select_package_label' => 'Select Package',
    'validity_label' => 'Validity',
    // Buy subscription Page
    'select_subscription_label' => 'Select Subscription',
    // Privacy policy and Terms
    'terms_label' => 'Terms & Conditions',
    'privacy_label' => 'Privacy Policy',
    'delete_account_label' => 'Delete Account',
];
