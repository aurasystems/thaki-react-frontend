<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Frontend Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used during authentication for various
      | messages that we need to display to the user. You are free to modify
      | these language lines according to your application's requirements.
      |
     */

    'app_name' => 'Thaki',
    'lang_name' => 'عربي',
    'lang_code' => 'ar',
    'lang_dir' => 'rtl',
    'currency_label' => 'ر.س',
    'copyright_text' => 'جميع الحقوق محفوظة',
    'error_label' => 'خطأ',
    'private_area_access_msg' => 'يجب عليك تسجيل الدخول أولا!',
    // Months
    'month_1' => 'يناير',
    'month_2' => 'فبراير',
    'month_3' => 'مارس',
    'month_4' => 'أبريل',
    'month_5' => 'مايو',
    'month_6' => 'يونيو',
    'month_7' => 'يوليو',
    'month_8' => 'أغسطس',
    'month_9' => 'سبتمر',
    'month_10' => 'أكتوبر',
    'month_11' => 'نوفمبر',
    'month_12' => 'ديسمبر',
    // Days
    'day_plural' => 'يوم/أيام',
    'hour_plural' => 'س',
    'hour_s_label' => 'ساعة/ت',
    // Form fields
    'name_field' => 'الأسم',
    'email_field' => 'البريد الإلكتروني',
    'phone_field' => 'الهاتف',
    'age_field' => 'العمر',
    'birthdate_field' => 'تاريخ الميلاد',
    'old_password_field' => 'كلمة المرور الحالية',
    'password_field' => 'كلمة المرور',
    'password_confirmation_field' => 'تأكيد كلمة المرور',
    'id_card_front_img_field' => 'Personal identity front image',
    'id_card_back_img_field' => 'Personal identity back image',
    // General buttons
    'submit_request_button' => 'تقديم الطلب',
    'confirm_button' => 'تأكيد',
    'cancel_button' => 'إلغاء',
    'set_default_button' => 'تعيين الإفتراضية',
    'select_button' => 'تحديد',
    'edit_button' => 'تعديل',
    'delete_button' => 'حذف',
    'back_button' => 'العودة',
    'save_button' => 'حفظ',
    'close_button' => 'إغلاق',
    // General Messages
    'confirm_action_msg' => 'تأكيد العملية',
    'confirm_delete_msg' => 'هل أنت متأكد من رغبتك في الحذف؟',
    'confirm_default_msg' => 'هل أنت متأكد من رغبتك في تعيين الإفتراضي؟',
    'confirm_buy_package_msg' => 'هل أنت متأكد من رغبتك في شراء هذه الباقة؟',
    'confirm_buy_subscription_msg' => 'هل أنت متأكد من رغبتك في شراء هذا الأشتراك؟',
    'confirm_cancel_booking_msg' => 'هل أنت متأكد من رغبتك في إلغاء حجزك؟',
    'saved_success_msg' => 'تم الحفظ بنجاح',
    'no_bookings_found' => 'ليس لديك حجوزات في الوقت الحالي',
    'no_cars_found_add_one' => 'لا يوجد سيارات لعرضها من فضلك قم بإضافة سيارة.',
    'no_cards_found_add_one' => 'لا يوجد بطاقات لعرضها من فضلك قم بإضافة بطاقة.',
    // Login Page
    'login_label' => 'تسجيل الدخول',
    'logout_label' => 'تسجيل الخروج',
    'login_desc' => 'تسجيل الدخول إلي حسابك',
    'login_button' => 'تسجيل الدخول',
    'login_proccess' => 'جاري تسجيل دخول...',
    'login_success_redirect' => 'تم تسجيل الدخول بنجاح، جاري إعادة التوجيه...',
    // Register Page
    'register_label' => 'تسجيل',
    'register_desc' => 'إنشاء حساب جديد',
    'register_button' => 'إنشاء حساب جديد',
    'register_success_redirect' => 'تم التسجيل بنجاح، جاري إعادة التوجيه...',
    // Profile Page
    'profile_label' => 'حسابي',
    'profile_button' => 'حسابي',
    // Dashboard Page
    'dashboard_label' => 'لوحة التحكم',
    'my_balance_label' => 'رصيدي',
    'my_bookings_label' => 'حجوزاتي',
    'current_balance_label' => 'الرصيد الحالي',
    'valid_from_label' => 'صالح من',
    'valid_till_label' => 'صالح حتي',
    'remaining_label' => 'المتبقي',
    'balance_detailes_label' => 'تفاصيل الرصيد',
    'from_label' => 'من',
    'to_label' => 'إلي',
    'park_for' => 'أحجز لمدة',
    'book_parking_button' => 'أحجز موقفك',
    'recharge_balance_button' => 'شحن رصيدي',
    'apply_subscription_button' => 'طلب إشتراك',
    'buy_subscription_button' => 'شراء إشتراك',
    'select_car_label' => 'حدد السيارة',
    'immediate_parking_button' => 'الحجز الأن',
    'immediate_parking_desc' => 'أحجز الأن',
    'schedule_parking_button' => 'جدولة الحجز',
    'schedule_parking_desc' => 'الحجز لاحقا',
    // My Parkings Page
    'my_parkings_label' => 'مواقف السيارات',
    'upcoming_label' => 'القادمة',
    'pending_label' => 'معلفة',
    'completed_label' => 'المكتملة',
    'cancelled_label' => 'الملغاء',
    'qr_button' => 'عرض QR',
    // My Cards Page
    'cards_label' => 'طرق الدفع',
    'card_holder_field' => 'أسم حامل البطاقة',
    'card_number_field' => 'رقم البطاقة',
    'card_expiry_field' => 'تاريخ الإنتهاء',
    'card_cvv_field' => 'رمز التحققCVV',
    'card_default_field' => 'الإفتراضية',
    'card_default_label' => 'الإفتراضية',
    'add_card_button' => 'أضف بطاقة',
    'add_card_label' => 'إضافة بطاقة',
    // Cars Page
    'cars_label' => 'السيارات الخاصة بي',
    'edit_car_label' => 'تعديل السيارة',
    'car_name_field' => 'أسم السيارة',
    'plate_number_en_field' => 'أرقام اللوحة المعدنية باللغة الانجليزية',
    'plate_number_ar_field' => 'أرقام اللوحة المعدنية باللغة العربية',
    'state_field' => 'الدولة',
    'make_field' => 'ماركة السيارة',
    'model_field' => 'موديل السيارة',
    'color_field' => 'لون السيارة',
    'year_field' => 'سنة الصنع',
    'car_default_field' => 'المفضلة',
    'car_default_label' => 'المفضلة',
    'add_car_button' => 'أضف سيارة',
    'add_car_label' => 'إضافة سيارة',
    // Buy package Page
    'buy_package_label' => 'شحن رصيدي',
    'select_package_label' => 'حدد الباقة',
    'validity_label' => 'الصلاحية',
    // Buy subscription Page
    'select_subscription_label' => 'حدد الاشتراك',
    // Privacy policy and Terms
    'terms_label' => 'سياسة الأستخدام',
    'privacy_label' => 'سياسة الخصوصية',
    'delete_account_label' => 'حذف حسابك',
];
