import React from 'react';
import {get_cookie, set_cookie, trans} from './components/helpers/general_helper';
import {Redirect, Route, withRouter} from 'react-router-dom';
// 3.1
let state_of_state = get_cookie();
if (!state_of_state) {
    let appState = {
        isLoggedIn: false,
        user: {}
    };
    state_of_state = set_cookie(appState);
    
}
let AppState = state_of_state;
// 3.2
const Auth = {
    isLoggedIn: AppState.isLoggedIn,
    user: AppState.user
};
// 3.3
const PrivateRoute = ({
    component: Component,
    path,
    ...rest
}) => ( <
    Route path = {
        path
    } {
        ...rest
    }
    render = {
        props => Auth.isLoggedIn ? ( <
            Component {
                ...props
            }
            />) : (<Redirect to={{
            pathname: "/login",
            state: {
                prevLocation: path,
                error: trans('front.private_area_access_msg'),
            },
        }
    }
    />
)
}
/>);
export default withRouter(PrivateRoute);
