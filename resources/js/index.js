import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom';
import ErrorBoundary from './components/ErrorBoundary';
import Header from './components/Header/Header';
import Side from './components/Side/Side';
import Footer from './components/Footer/Footer';
import {get_cookie, trans} from './components/helpers/general_helper';

import Main from './Router';
class Index extends Component {
    constructor() {
        super();
        let AppState = get_cookie();
        this.state = AppState ? {isLoggedIn: AppState.isLoggedIn, user: AppState.user} : {isLoggedIn: false, user: {}};
    }
    render() {
        return (
                <BrowserRouter>
                    <Header userData={this.state.user} userIsLoggedIn={this.state.isLoggedIn}/>
                    <main className="py-4">         
                        <div className="container">
                            <div className="row row-offcanvas">
                                <div className="col mb-5">
                                    <ErrorBoundary>
                                        <Route component={Main} />
                                    </ErrorBoundary>
                                </div>
                                {this.state.isLoggedIn ? <Side /> : ''}
                            </div>
                        </div>
                    </main>
                    <Footer />
                </BrowserRouter>
                );
    }
}
ReactDOM.render(<Index/>, document.getElementById('app'));