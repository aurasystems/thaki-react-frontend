import React from "react";
import { BrowserRouter, Link, Route, Switch } from "react-router-dom";
import {url} from "./components/helpers/general_helper";
import {param} from "./components/helpers/api_helper";
import Home from "./components/Home/Home";
import Login from "./views/Login/Login";
import Register from "./views/Register/Register";
import NotFound from "./views/NotFound/NotFound";

// Authenticated User route handler
import PrivateRoute from "./PrivateRoute";
// Import dashboard class
import Dashboard from "./views/user/Dashboard/Dashboard";
// Import car classes
import Cars from "./views/user/Cars/Cars"; 
import AddCar from "./views/user/Cars/AddCar"; 
import EditCar from "./views/user/Cars/EditCar"; 
// Import card classes
import Cards from "./views/user/Cards/Cards"; 
import AddCard from "./views/user/Cards/AddCard"; 
// Import packages class
import BuyPackage from "./views/user/Packages/BuyPackage"; 
// Import subscription class
import ApplySubscription from "./views/user/Subscription/ApplySubscription"; 
import BuySubscription from "./views/user/Subscription/BuySubscription"; 
// Import booking class
import MyBookings from "./views/user/Bookings/MyBookings"; 
// Import profile class
import Profile from "./views/user/Profile"; 
// Import logout class
import LogOut from "./views/user/LogOut"; 

const Main = () => (
    <Switch>
        {/*User might LogIn*/}
        <Route exact path={url('/')} component={Home} />
        {/*User will LogIn*/}
        <Route path={url(param('login_url'))} component={Login} />
        <Route path={url(param('register_url'))} component={Register} />
        {/* User is LoggedIn*/}
        <Route path={url(param('logout_url'))} component={LogOut} />
        <PrivateRoute path={url('/dashboard')} component={Dashboard} />
        <PrivateRoute path={url('/profile')} component={Profile} />
        
        <PrivateRoute path={url('/recharge_balance')} component={BuyPackage} exact={true}/>
        <PrivateRoute path={url('/apply_subscription')} component={ApplySubscription} exact={true}/>
        <PrivateRoute path={url('/buy_subscription')} component={BuySubscription} exact={true}/>
        
        <PrivateRoute path={url('/bookings')} component={MyBookings} exact={true}/>

        <PrivateRoute path={url('/cars')} component={Cars} exact={true}/>
        <PrivateRoute path={url('/cars/add')} component={AddCar} exact={true}/>
        <PrivateRoute path={url('/cars/edit/:id')} component={EditCar} exact={true} />
        
        <PrivateRoute path={url('/cards')} component={Cards} exact={true}/>
        <PrivateRoute path={url('/cards/add')} component={AddCard} exact={true}/>
        
        {/*Page Not Found*/}
        <Route component={NotFound} />
    </Switch>
);
export default Main;
