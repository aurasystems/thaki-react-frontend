import React, {Component} from 'react'
import {Link, withRouter} from 'react-router-dom';
import Cookies from 'js-cookie'
import {trans, url} from '../helpers/general_helper';
import {param} from '../helpers/api_helper';

class Header extends Component {
// 1.1
    constructor(props) {
        super(props);
        this.state = {
            lang: document.documentElement.lang,
            user: props.userData,
            isLoggedIn: props.userIsLoggedIn
        };
    }
// 1.2
    render() {
        return (
                <nav className="navbar navbar-expand-md navbar-light bg-white shadow-sm">
                    <div className="container">
                        <Link className="navbar-brand" to={url('/')}><img src={url('/images/logo.png')} /></Link>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav mr-auto">
                
                            </ul>
                
                            <ul className="navbar-nav ml-auto">
                                {!this.state.isLoggedIn ?
                                    <li className="nav-item"><Link className="nav-link" to={url(param('login_url'))}>{trans('front.login_label')}</Link></li> : ""
                                }
                                {!this.state.isLoggedIn ?
                                    <li className="nav-item"><Link className="nav-link" to={url(param('register_url'))}>{trans('front.register_label')}</Link></li> : ""
                                }
                
                                {this.state.isLoggedIn ?
                                    <li className="nav-item dropdown">
                                        <a id="navbarDropdown" className="nav-link dropdown-toggle" href="#!" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre="true">
                                            <img src={this.state.user.profile_picture ? this.state.user.profile_picture : url('/images/default_avatar.png')} width="32" /> <span id="header_name">{this.state.user.name}</span>
                                        </a>
                    
                                        <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">                    
                                            <Link className="dropdown-item" to={url('/profile')} >{trans('front.profile_button')}</Link>
                                            <Link className="dropdown-item" to={url(param('logout_url'))} >{trans('front.logout_label')}</Link>
                                        </div>
                                    </li>
                                            : ""
                                }
                                <li className="nav-item">
                                    {this.state.lang === 'en' ? <a className="nav-link btn-primary rounded-sm text-white" href={url('/lang/ar')}>عربي</a> : <a className="nav-link btn-primary rounded-sm text-white" href={url('/lang/en')}>English</a>}
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>

                )
    }
}
export default withRouter(Header)