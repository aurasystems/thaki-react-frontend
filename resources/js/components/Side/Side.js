import React, {Component} from 'react'
import {Link, withRouter} from 'react-router-dom';
import {trans, url} from '../helpers/general_helper';

class Side extends Component {
    render() {
        return (
                <div className="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
                    <div className="list-group mb-5">
                        <Link to={url('/dashboard')} id="dashboard_side" className="list-group-item">{trans('front.dashboard_label')}</Link>
                        <Link to={url('/bookings')} id="bookings_side" className="list-group-item">{trans('front.my_parkings_label')}</Link>
                        <Link to={url('/cars')} id="cars_side" className="list-group-item">{trans('front.cars_label')}</Link>
                        <Link to={url('/cards')} id="cards_side" className="list-group-item">{trans('front.cards_label')}</Link>
                    </div>
                </div>
                )
    }
}
export default withRouter(Side)