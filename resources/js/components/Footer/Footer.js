import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import {trans, url} from '../helpers/general_helper';

const Footer = () => {
    return (
            <footer className="bg-primary text-center text-lg-start fixed-bottom">
                <div className="text-center p-3">
                    {trans('front.copyright_text')} © <span dir="ltr">{new Date().getFullYear()}</span> <Link className="text-warning" to={url('/')}>{trans('front.app_name')}.</Link>
                </div>
            </footer>
            )
};
export default withRouter(Footer)