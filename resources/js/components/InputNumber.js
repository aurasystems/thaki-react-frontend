import React, {Component} from 'react';
class InputNumber extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: this.props.min,
        };
        // Bind
        this.increment = this.increment.bind(this);
        this.decrement = this.decrement.bind(this);
    }

    get value() {
        return this.state.value;
    }

    increment() {
        const {max} = this.props;
        if (typeof max === 'number' && this.value >= max)
            return;
        this.setState({value: this.value + 1});
    }

    decrement() {
        const {min} = this.props;
        if (typeof min === 'number' && this.value <= min)
            return;
        this.setState({value: this.value - 1});
    }

    render() {
        return (
                <div className="input-group input-group">
                    <div className="input-group-prepend">
                        <button type="button" className="btn btn-primary btn-sm" onClick={this.decrement}>&minus;</button>
                    </div>
                    <input type="text" className="form-control text-center" value={this.value} readOnly/>
                    <div className="input-group-append">
                        <button type="button" className="btn btn-primary btn-sm" onClick={this.increment}>&#43;</button>
                    </div>
                </div>
                );
    }
}
export default InputNumber