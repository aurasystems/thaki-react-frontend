import React, {Component} from 'react'
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import {get_cookie, trans, unset_side_active, page_title} from '../../components/helpers/general_helper';
import {api_url} from '../../components/helpers/api_helper';

class Home extends Component {
    constructor() {
        super();
        page_title();
        unset_side_active();
        let state = {
            isLoggedIn: false,
            user: {}
        };
        let AppState = get_cookie();
        if (AppState) {
            state.isLoggedIn = AppState.isLoggedIn;
            state.user = AppState.user;

        }
        this.state = state;
    }
    render() {
        return null
    }
}
export default Home