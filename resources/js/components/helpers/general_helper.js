import config from "../../config.json";
import Cookies from 'js-cookie';

export const url = (u) => {
    return config.APP_PATH + (u ? u : '');
}
export const trans = (key, replace = {}) => {
    let translation = key.split('.').reduce((t, i) => t[i] || '', window.translations);

    for (var placeholder in replace) {
        translation = translation.replace(`:${placeholder}`, replace[placeholder]);
    }

    return translation;
}

export const trans_choice = (key, count = 1, replace = {}) => {
    let translation = key.split('.').reduce((t, i) => t[i] || '', window.translations).split('|');

    translation = count > 1 ? translation[1] : translation[0];

    for (var placeholder in replace) {
        translation = translation.replace(`:${placeholder}`, replace[placeholder]);
    }

    return translation;

}

export const side_active = (id) => {
    if (!$('#' + id + '_side').hasClass('active')) {
        // Unset active sidebar
        $("#sidebar a").each(function () {
            if ($(this).hasClass('active'))
                $(this).removeClass('active');

        });
    }
    setTimeout(() => {
        // Set active sidebar
        $('#' + id + '_side').addClass('active');
    }, 50);

}

/* Unset active sidebar */
export const unset_side_active = () => {
    $("#sidebar .list-group a").each(function () {
        if ($(this).hasClass('active'))
            $(this).removeClass('active');

    });

}

/* Set page title */
export const page_title = (t, f) => {
    let page_title = f ? t : trans('front.app_name') + (t ? ' | ' + trans(t) : '');
    document.title = page_title;
}

/* Get cookie */
export const get_cookie = (n) => {
    let state = Cookies.get(n ? n : config.COOKIE_NAME);
    return state ? JSON.parse(state) : {};
}

// Set cookie
export const set_cookie = (v, n) => {
    let params = {
        path: config.COOKIE_PATH,
        domain: config.COOKIE_DOMAIN,
        secure: config.COOKIE_SECURE
    };
    Cookies.set(n ? n : config.COOKIE_NAME, JSON.stringify(v), params);
    let state = Cookies.get(n ? n : config.COOKIE_NAME);
    return state ? JSON.parse(state) : {};
}

// Display flash message
export const flashMessage = (p, t, dur, c) => {
    let el = $('.' + (c ? c : 'flash_message'));
    if (el) {
        el.empty();
        el.html('<h5 class="alert alert-' + t + '">' + p + '</h5>');
        if (el.is(":hidden"))
            el.fadeIn();
        setTimeout(() => {
            el.fadeOut('slow');
        }, dur);
    }

}

// Return years array
export const years = (plus = 10, trim) => {
    // Get current year
    let current_year = (new Date()).getFullYear();
    // Return array of years
    return Array.from(new Array(plus), (val, index) => (trim ? (index + current_year).toString().substring(2, 4) : (index + current_year)));
}

// Return months array
export const months = (year) => {
    let months = [];
    let newDate = new Date();
    // Get current month
    let current_year = newDate.getFullYear();
    if (parseInt(year) === current_year) {
        // Get current month
        let current_month = newDate.getMonth() + 1;
        // Populate month in array
        for (let i = current_month; i <= 12; i++) {
            months.push({pfx: i < 10 ? '0' : '', m: i, n: trans('front.month_' + i)});
        }
    } else {
        // Populate month in array
        for (let i = 1; i <= 12; i++) {
            months.push({pfx: i < 10 ? '0' : '', m: i, n: trans('front.month_' + i)});
        }
    }
    // Return array of months
    return months;
}

// Return days array
export const days = (month, year) => {
    let days = [];
    if (month === 2) {
        if (year % 4 === 0) {
            for (let i = 1; i <= 29; i++) {
                days.push(i);
            }
        } else {
            for (let i = 1; i <= 28; i++) {
                days.push(i);
            }
        }
    } else if (month === 4 || month === 6 || month === 9 || month === 11) {
        for (let i = 1; i <= 30; i++) {
            days.push(i);
        }
    } else {
        for (let i = 1; i <= 31; i++) {
            days.push(i);
        }
    }
    return days;
}