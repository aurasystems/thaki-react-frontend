import data from "./data_params.json";
import config from "../../config.json";

/* Get data paramaters */
export const param = (u) => {
    return data[u] ? data[u] : '';
}

export const api_url = (u) => {
    return config.SERVER_URL + 'api/' + (u ? u : '');
}