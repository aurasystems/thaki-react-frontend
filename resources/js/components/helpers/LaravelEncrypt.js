import CryptoJS from "crypto-js";
import {serialize} from "php-serialization";
import Encrypter from "laravel-crypton";

const LaravelEncrypt = function (key) {
    this.key = key;
    this.cipher = 'AES-256-CBC';
}

LaravelEncrypt.prototype.decrypt = function (encryptStr) {
    // Laravel creates a JSON to store iv, value and a mac and base64 encodes it.
    // So let's base64 decode the string to get them.
    let encrypted = atob(encryptStr);
    encrypted = JSON.parse(encrypted);

    // IV is base64 encoded in Laravel, expected as word array in cryptojs
    const iv = CryptoJS.enc.Base64.parse(encrypted.iv);

    // Value (chipher text) is also base64 encoded in Laravel, same in cryptojs
    const value = encrypted.value;


    // Key is base64 encoded in Laravel, word array expected in cryptojs
    let key = CryptoJS.enc.Base64.parse(this.key);

    // Decrypt the value, providing the IV. 
    var decrypted = CryptoJS.AES.decrypt(value, key, {
        iv: iv
    });

    // CryptoJS returns a word array which can be converted to string like this
    decrypted = decrypted.toString(CryptoJS.enc.Utf8);
    return decrypted;
};


LaravelEncrypt.prototype.encrypts = function (value, serialize = true) {
    let iv = CryptoJS.lib.WordArray.random(16),
            key = CryptoJS.enc.Base64.parse(this.key);
    
        // First we will encrypt the value using OpenSSL. After this is encrypted we
        // will proceed to calculating a MAC for the encrypted value so that this
        // value can be verified later as not having been changed by the users.
        const cipher = CryptoJS.createCipheriv(this.cipher, this.key, iv);

        try {
            value = Buffer.concat([
                cipher.update(value),
                cipher.final()
            ]).toString('base64');
        } catch (e) {
            throw new EncryptError('Could not encrypt the data.');
        }

        // Once we get the encrypted value we'll go ahead and base64.encode the input
        // vector and create the MAC for the encrypted value so we can then verify
        // its authenticity. Then, we'll JSON the data into the "payload" array.
        let mac = this.hash(iv = Buffer.from(iv, 'binary').toString('base64'), value);
        return Buffer.from(JSON.stringify({iv, value, mac}), 'utf8').toString('base64');
};

LaravelEncrypt.prototype.encrypt = function (data, serialize = true) {
    let iv = CryptoJS.lib.WordArray.random(16),
            key = CryptoJS.enc.Base64.parse(this.key);
    let options = {
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    };
    let encrypted = CryptoJS.AES.encrypt(data, key,options);
    encrypted = encrypted.toString();
    iv = CryptoJS.enc.Base64.stringify(iv);
    let result = {
        iv: iv,
        value: encrypted,
        mac: CryptoJS.HmacSHA256(iv + encrypted, key).toString()
    };
    result = JSON.stringify(result);
    return result;
};
export default LaravelEncrypt;
