import React, {Component} from 'react';
import {Link, Redirect, withRouter} from 'react-router-dom';
import FlashMessage from 'react-flash-message';
import {get_cookie, set_cookie, trans, page_title, url} from '../../components/helpers/general_helper';
import {api_url, param} from '../../components/helpers/api_helper';
class LoginContainer extends Component {
    constructor(props) {
        super(props);
        page_title('front.login_label');
        let state = {
            isLoggedIn: false,
            formSubmitting: false,
            error: '',
            errorMessage: '',
            user: {
                email: '',
                password: '',
            },
            headers: {
                headers: {
                    [param('localization_header')]: document.documentElement.lang
                }
            },
            redirect: props.redirect,
        };
        let AppState = get_cookie();
        if (AppState) {
            state.isLoggedIn = AppState.isLoggedIn;
        }
        this.state = state;
        // bind
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    componentDidMount() {
        if (this.state.isLoggedIn) {
            return this.props.history.push(url('/dashboard'));
        }
        const {prevLocation} = this.state.redirect.state || {prevLocation: {pathname: '/dashboard'}};
        if (prevLocation && this.state.isLoggedIn) {
            return this.props.history.push(prevLocation);
        }
    }
    handleSubmit(e) {
        e.preventDefault();
        this.setState({formSubmitting: true});
        let userData = this.state.user;
        axios.post(api_url(`auth/login`), userData, this.state.headers).then(response => {
            return response;
        }).then(json => {
            if (json.data.success) {
                let responseData = json.data[param('data')];
                let userData = {
                    id: responseData[param('user')][param('id')],
                    name: responseData[param('user')][param('name')],
                    email: responseData[param('user')][param('email')],
                    phone: responseData[param('user')][param('phone')],
                    age: responseData[param('user')][param('age')],
                    is_approved: responseData[param('user')].is_approved,
                    profile_picture: responseData[param('user')][param('profile_picture')],
                    token_type: responseData.token_type,
                    access_token: responseData.access_token,
                    expires_at: responseData.expires_at,
                };
                let appState = {
                    isLoggedIn: true,
                    user: userData
                };
                set_cookie(appState);
                this.setState({
                    isLoggedIn: appState.isLoggedIn,
                    user: appState.user,
                    error: ''
                });
                location.reload()
            } else {
                this.setState({
                    error: 'Our System Failed To Register Your Account!',
                    errorMessage: 'Our System Failed To Register Your Account!',
                    formSubmitting: false
                })
            }
        }).
                catch(error => {
                    if (error.response) {
                        // The request was made and the server responded with a status code that falls out of the range of 2xx
                        let err = error.response.data;
                        this.setState({
                            error: err.message ? err.message.toString() : '',
                            errorMessage: err.errors ? err.errors : [],
                            formSubmitting: false
                        })
                    } else if (error.request) {
                        // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
                        let err = error.request;
                        this.setState({
                            error: err ? err.toString() : '',
                            formSubmitting: false
                        })
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        let err = error.message;
                        this.setState({
                            error: err ? err.toString() : '',
                            formSubmitting: false
                        })
                    }
                }).finally(this.setState({error: ''}));
    }

    // handle data change
    handleChange(el) {
        let inputName = el.target.id;
        let inputValue = el.target.type === 'checkbox' ? (el.target.checked ? 1 : 0) : el.target.value;
        let statusCopy = Object.assign({}, this.state);
        statusCopy.user[inputName] = inputValue;
        this.setState(statusCopy);
    }
    render() {
        let errorMessage = this.state.errorMessage;
        let arr = [];
        Object.values(errorMessage).forEach((value) => (
                    arr.push(value)
                    ));
        return (
                <div className="row">
                    <div className="offset-xl-3 col-xl-6 offset-lg-1 col-lg-10 col-md-12 col-sm-12 col-12 ">
                        <h2 className="text-center mb-4 text-primary">{trans('front.login_desc')}</h2>
                        {this.state.isLoggedIn ? <FlashMessage duration={60000} persistOnHover={true}>
                            <h5 className={"alert alert-success"}>{trans('front.login_success_redirect')}</h5></FlashMessage> : ''}
                        {this.state.error ? <FlashMessage duration={900000} persistOnHover={true}>
                            <h5 className={"alert alert-danger"}>{trans('front.error_label')}: {this.state.error}</h5>
                            <ul>
                                {arr.map((item, i) => (
                                                    <li key={i}><h5 style={{color: 'red'}}>{item}</h5></li>
                                                                    ))}
                            </ul></FlashMessage> : ''}
                        <form onSubmit={this.handleSubmit} autoComplete="off">
                            <div className="form-group">
                                <input id="email" type="email" name="email" placeholder={trans('front.email_field')} className="form-control rounded-pill" required onChange={this.handleChange}/>
                            </div>
                            <div className="form-group">
                                <input id="password" type="password" name="password" placeholder={trans('front.password_field')} className="form-control rounded-pill" required onChange={this.handleChange}/>
                            </div>
                            <button disabled={this.state.formSubmitting} type="submit" name="singlebutton" className="btn btn-primary rounded-pill btn-block mb-10"> {this.state.formSubmitting ? trans('front.login_proccess') : trans('front.login_button')} </button>
                        </form>
                    </div>
                </div>
                )
    }
}
export default withRouter(LoginContainer);