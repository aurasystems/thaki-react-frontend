import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import ReactDOM from 'react-dom';
import FlashMessage from 'react-flash-message';
import {get_cookie, set_cookie, trans, page_title, url} from '../../components/helpers/general_helper';
import {api_url, param} from '../../components/helpers/api_helper';
class RegisterContainer extends Component {
    // 2.1
    constructor(props) {
        super(props);
        // Set page title
        page_title('front.register_label');
        let state = {
            isLoggedIn: false,
            isRegistered: false,
            user: {},
            headers: {
                headers: {
                    'X-localization': document.documentElement.lang
                }
            },
            error: '',
            errorMessage: '',
            formSubmitting: false,
            redirect: props.redirect,
        };
        let AppState = get_cookie();
        if (AppState) {
            state.isLoggedIn = AppState.isLoggedIn;
        }
        this.state = state;
        // bind
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    // 2.3
    componentDidMount() {
        if (this.state.isLoggedIn) {
            return this.props.history.push(url('/dashboard'));
        }
        const {prevLocation} = this.state.redirect.state || {prevLocation: {pathname: '/dashboard'}};
        if (prevLocation && this.state.isLoggedIn) {
            return this.props.history.push(prevLocation);
        }
    }
    // 2.4
    handleSubmit(e) {
        e.preventDefault();
        this.setState({formSubmitting: true});
        ReactDOM.findDOMNode(this).scrollIntoView();
        let userData = this.state.user;
        axios.post(api_url(`auth/register`), userData, this.state.headers)
                .then(response => {
                    return response;
                }).then(json => {
            if (json.data.success) {
                let responseData = json.data[param('data')];
                let userData = {
                    id: responseData[param('user')].id,
                    name: responseData[param('user')].name,
                    email: responseData[param('user')].email,
                    phone: responseData[param('user')].phone,
                    age: responseData[param('user')].age,
                    profile_picture: responseData[param('user')].profile_picture,
                    is_approved: responseData[param('user')].is_approved,
                    token_type: responseData.token_type,
                    access_token: responseData.access_token,
                    expires_at: responseData.expires_at,
                };
                let appState = {
                    isRegistered: true,
                    isLoggedIn: true,
                    user: userData
                };
                set_cookie(appState);
                this.setState({
                    isRegistered: appState.isRegistered,
                    isLoggedIn: appState.isLoggedIn,
                    user: appState.user
                });
                location.reload()
            } else {
                alert(`Our System Failed To Register Your Account!`);
            }
        }).catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code that falls out of the range of 2xx
                let err = error.response.data;
                this.setState({
                    error: err.message,
                    errorMessage: err.errors ? err.errors : [],
                    formSubmitting: false
                })
            } else if (error.request) {
                // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
                let err = error.request;
                this.setState({
                    error: err,
                    formSubmitting: false
                })
            } else {
                // Something happened in setting up the request that triggered an Error
                let err = error.message;
                this.setState({
                    error: err,
                    formSubmitting: false
                })
            }
        }).finally(this.setState({error: ''}));
    }
    // handle data change
    handleChange(el) {
        let inputName = el.target.id;
        let inputValue = el.target.type === 'checkbox' ? (el.target.checked ? 1 : 0) : el.target.value;
        let statusCopy = Object.assign({}, this.state);
        statusCopy.user[inputName] = inputValue;
        this.setState(statusCopy);
    }
    render() {
        // 2.6
        let errorMessage = this.state.errorMessage;
        let arr = [];
        Object.values(errorMessage).forEach((value) => (
                    arr.push(value)
                    ));
        return (
                <div className="row">
                    <div className="offset-xl-3 col-xl-6 offset-lg-1 col-lg-10 col-md-12 col-sm-12 col-12 ">
                        <h2 className="text-center mb-4 text-primary">{trans('front.register_desc')}</h2>
                        {this.state.isRegistered ? <FlashMessage duration={60000} persistOnHover={true}>
                            <h5 className={"alert alert-success"}>{trans('front.register_success_redirect')}</h5></FlashMessage> : ''}
                        {this.state.error ? <FlashMessage duration={900000} persistOnHover={true}>
                            <h5 className={"alert alert-danger"}>{trans('front.error_label')}: {this.state.error}</h5>
                            <ul>
                                {arr.map((item, i) => (
                                                    <li key={i}><h5 style={{color: 'red'}}>{item}</h5></li>
                                                                    ))}
                            </ul></FlashMessage> : ''}
                        <form onSubmit={this.handleSubmit} autoComplete="off">
                            <div className="form-group">
                                <input id="name" type="text" placeholder={trans('front.name_field')} className="form-control rounded-pill" required onChange={this.handleChange}/>
                            </div>
                            <div className="form-group">
                                <input id="email" type="email" name="email" placeholder={trans('front.email_field')} className="form-control rounded-pill" required onChange={this.handleChange}/>
                            </div>
                            <div className="form-group">
                                <input id="phone" type="text" name="phone" placeholder={trans('front.phone_field')} className="form-control rounded-pill" required onChange={this.handleChange}/>
                            </div>
                            <div className="form-group">
                                <input id="age" type="number" name="age" placeholder={trans('front.age_field')} className="form-control rounded-pill" required onChange={this.handleChange}/>
                            </div>
                            <div className="form-group">
                                <input id="password" type="password" name="password" placeholder={trans('front.password_field')} className="form-control rounded-pill" required onChange={this.handleChange}/>
                            </div>
                            <div className="form-group">
                                <input id="password_confirmation" type="password" name="password_confirmation" placeholder={trans('front.password_confirmation_field')} className="form-control rounded-pill" required onChange={this.handleChange} />
                            </div>
                            <button type="submit" name="singlebutton" className="btn btn-primary rounded-pill btn-block mb-10" disabled={this.state.formSubmitting ? "disabled" : ""}>{trans('front.register_button')}</button>
                        </form>
                    </div>
                </div>
                )
    }
}
// 2.8
export default withRouter(RegisterContainer);