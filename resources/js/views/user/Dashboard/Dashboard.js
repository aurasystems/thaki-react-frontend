import React, { Component, useState } from 'react';
import {Link} from 'react-router-dom';
import {get_cookie, set_cookie, trans, side_active, page_title, flashMessage, url} from '../../../components/helpers/general_helper';
import {param, api_url} from '../../../components/helpers/api_helper';
import { Modal, Button } from "react-bootstrap";
import InputNumber from "../../../components/InputNumber";
import DateTimePicker from 'react-datetime-picker';
import moment from 'moment';

class Home extends Component {

    constructor(props) {
        super(props);
        // Set page title
        page_title('front.dashboard_label');
        // Set Sidebar active
        side_active('dashboard');
        let state = {
            lang: document.documentElement.lang,
            loading: false,
            headers: {},
            isOpenBalance: false,
            isOpenCars: false,
            isOpenBooking: false,
            booking_type: "1",
            datetime: new Date(),
            car_id: '',
            error: '',
            errorMessage: [],
            user: {
                is_approved: ''
            },
            data: {
                bookings: [],
                packages: [],
                balance: {
                    balance_value: 0,
                    balance_string: '',
                    vaild_till: '',
                }
            }
        };
        let AppState = get_cookie();
        if (AppState) {
            state.user = AppState.user;
            state.headers = {
                headers: {
                    'Authorization': AppState.user.token_type + ' ' + AppState.user.access_token,
                    [param('localization_header')]: document.documentElement.lang
                }
            };
        }
        this.state = state;
        // Bind
        this.handleChange = this.handleChange.bind(this);
        this.handleDTPicker = this.handleDTPicker.bind(this);
        this.bookParking = this.bookParking.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }
    componentDidMount() {
        this.getData();
    }
    openModal(type) {
        if (type === 'Booking')
            this.setState({isOpenCars: false});
        this.setState({['isOpen' + type]: true});
    }
    closeModal(type) {
        if (type === 'Booking')
            this.setState({['booking_type']: "1"});

        this.setState({['isOpen' + type]: false});
    }
    // handle date picker
    handleDTPicker(date) {
        this.setState({
            datetime: date
        });
    }
    // handle data change
    handleChange(el) {
        let inputName = el.target.name;
        let inputValue = el.target.value;
        let statusCopy = Object.assign({}, this.state);
        statusCopy[inputName] = inputValue;
        this.setState(statusCopy);
    }
    /* get dashboard widget data from backend */
    getData() {
        axios.get(api_url(`dashboard`), this.state.headers).
                then((response) => {
                    if (response.data[param('success')]) {
                        let responseData = response.data[param('data')];
                        let existing = get_cookie();

                        existing.user.name = responseData.info[param('user')][param('name')];
                        existing.user.email = responseData.info[param('user')][param('email')];
                        existing.user.phone = responseData.info[param('user')][param('phone')];
                        existing.user.age = responseData.info[param('user')][param('age')];
                        existing.user.is_approved = responseData.info[param('user')].is_approved;
                        set_cookie(existing);
                        this.setState({
                            data: responseData[param('widgets')],
                            user: existing.user
                        });
                    }
                }).
                catch(error => {
                    if (error.response) {
                        if (error.response.status === 401) {
                            this.props.history.push(url(param('logout_url')));
                        }
                        // The request was made and the server responded with a status code that falls out of the range of 2xx
                        let err = error.response.data;
                        this.setState({
                            error: err.message ? err.message : '',
                            errorMessage: err.errors ? err.errors : [],
                        })
                    } else if (error.request) {
                        // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
                        let err = error.request;
                        this.setState({
                            error: err
                        });
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        let err = error.message;
                        this.setState({
                            error: err
                        })
                    }
                });
    }
    // Book parking from backend
    bookParking(e) {
        // stop browser's default behaviour of reloading on form submit
        e.preventDefault();
        this.setState({loading: true});
        let params = {car_id: this.state.car_id, duration: this.refs.duration.value}
        params.datetime = this.state.booking_type === "2" ? moment(this.state.datetime).format('YYYY-MM-DD HH:mm:ss') : '';
        axios.post(api_url(`booking`), params, this.state.headers).
                then((response) => {
                    this.setState({loading: false});
                    if (response.data.success) {
                        // Display success message
                        flashMessage(response.data.message, 'success', 3000);
                        // Update data
                        this.getData();
                        // Close booking model
                        this.closeModal('Booking');
                    }
                }).
                catch(error => {
                    this.setState({loading: false});
                    if (error.response) {
                        if (error.response.status === 401) {
                            this.props.history.push(url(param('logout_url')));
                        }
                        // The request was made and the server responded with a status code that falls out of the range of 2xx
                        let err = error.response.data;
                        this.setState({
                            error: err.message ? err.message.toString() : '',
                            errorMessage: err.errors ? err.errors : [],
                        })
                    } else if (error.request) {
                        // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
                        let err = error.request;
                        this.setState({
                            error: err ? err.toString() : ''
                        });
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        let err = error.message;
                        this.setState({
                            error: err ? err.toString() : ''
                        });
                    }
                });
    }

    // Render subscription button according to user approval status
    renderSubscriptionButton() {
        if (this.state.user.is_approved === 0) {
            return (
                    <Link to={url('/apply_subscription')} className="btn btn-warning btn-block rounded-pill">{trans('front.apply_subscription_button')}</Link>
                    );
        } else if (this.state.user.is_approved === 1) {
            return (
                    <Link to={url('/buy_subscription')} className="btn btn-warning btn-block rounded-pill">{trans('front.buy_subscription_button')}</Link>
                    );
        } else {
            return null;
        }
    }

    /* Render */
    render() {

        let bookings = this.state.data.bookings;
        let arr = [];
        Object.values(bookings).forEach((value) => (
                    arr.push(value)
                    ));
        let packages = this.state.data.packages;
        let parr = [];
        Object.values(packages).forEach((value) => (
                    parr.push(value)
                    ));
        return (
                <div className="row">
                    <div className="col-12">
                        <h5 className="text-primary">{trans('front.my_bookings_label')}</h5>
                        <div className="flash_message"></div>
                
                        <div id="booking-carousel" className="carousel slide" data-ride="carousel">
                            <ol className="carousel-indicators">
                                {arr.map((item, i) => (
                                        <li data-target="#booking-carousel" key={"ind_" + i.toString()} data-slide-to={i.toString()} className={i == 0 ? 'active' : ''}></li>
                                                ))}
                            </ol>
                            <div className="carousel-inner">
                                {arr.length > 0 ? arr.map((item, i) => (
                                        <div className={'carousel-item ' + (i == 0 ? 'active' : '')} key={i.toString()} >
                                            <div className="card text-white bg-primary d-block w-100 mb-3">
                                                <div className="card-body">
                                                    <div className="card-text">
                                                        <div className="row align-items-center h-100">
                                                            <div className="col-4">
                                                                <h5 className="card-title">{item.car.plate_number_en}</h5>
                                                                <span>{item.car.make + " - " + item.car.model}</span>
                                                            </div>
                                                            <div className="col-4">
                                                                {trans('front.from_label')}<br /><span dir="ltr">{item.date_from}</span><br /><span dir="ltr">{item.time_from}</span>
                                                            </div>
                                                            <div className="col-4">
                                                                {trans('front.to_label')}<br /><span dir="ltr">{item.date_to}</span><br /><span dir="ltr">{item.time_to}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                                )) :
                                    <div className="carousel-item active">
                                        <div className="card text-white bg-primary d-block w-100 mb-3">
                                            <div className="card-body">
                                                <div className="card-text">
                                                    <div className="row align-items-center h-100">
                                                        <div className="col-12">
                                                            <h5 className="card-title">{trans('front.no_bookings_found')}</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                
                    <div className="col-12 mt-4">
                        <div className="card">
                            <div className="card-body">
                                <h5 className="card-title text-primary">{trans('front.my_balance_label')}</h5>
                                <div className="row card-text align-items-center h-100">
                                    <div className="col-6">
                                        <span className="text-primary">{trans('front.current_balance_label')}</span>
                                        {': '}{this.state.data.balance.balance_value + ' ' + this.state.data.balance.balance_string}
                                        {
                                            // Check if balance value to display detailes button
                                            this.state.data.balance.balance_value ?
                                    <React.Fragment>
                                        <br />
                                        <Button variant="primary" onClick={() => this.openModal('Balance')}>
                                            {trans('front.balance_detailes_label')}
                                        </Button>
                                    </React.Fragment>
                                                    : ''}
                
                                    </div>
                                    <div className="col-6">
                                        <Link to={url('/recharge_balance')} className="btn btn-primary btn-block rounded-pill">{trans('front.recharge_balance_button')}</Link>
                                        {this.renderSubscriptionButton()}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 mt-4">
                        <button className="btn btn-primary btn-block rounded-pill p-2" onClick={() => this.openModal('Cars')}>{trans('front.book_parking_button')}</button>
                    </div>
                    <Modal 
                        show={this.state.isOpenBalance} 
                        onHide={() => this.closeModal('Balance')}
                        aria-labelledby="contained-modal-title-vcenter"
                        centered
                        >
                        <Modal.Header className="bg-primary text-white" closeButton>
                            <Modal.Title>{trans('front.balance_detailes_label')}</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <table className="table">
                                <thead className="thead-light">
                                    <tr>
                                        <th className="text-center text-primary">{trans('front.hour_s_label')}</th>
                                        <th className="text-center text-primary">{trans('front.remaining_label')}</th>
                                        <th className="text-center text-primary">{trans('front.valid_from_label')}</th>
                                        <th className="text-center text-primary">{trans('front.valid_till_label')}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {parr.length > 0 ? parr.map((item, i) => (
                                        <tr key={"package_" + item.id}>
                                            <td className="text-center">{item.hours}</td>
                                            <td className="text-center">{item.remaining_hours}</td>
                                            <td className="text-center">{item.start_date}</td>
                                            <td className="text-center">{item.end_date}</td>
                                        </tr>
                                                    )) : ''}  
                                </tbody>
                            </table>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button className="btn-primary btn-block" variant="secondary" onClick={() => this.closeModal('Balance')}>
                                {trans('front.close_button')}
                            </Button>
                        </Modal.Footer>
                    </Modal>
                    <Modal 
                        show={this.state.isOpenCars} 
                        onHide={() => this.closeModal('Cars')}
                        aria-labelledby="contained-modal-title-vcenter"
                        centered
                        >
                        <Modal.Header className="bg-primary text-white" closeButton>
                            <Modal.Title>{trans('front.select_car_label')}</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="row align-items-center h-100">
                                <div className="col-12">
                                    <div className="btn-group btn-group-vertical btn-group-toggle w-100" data-toggle="buttons">
                                        {this.state.data.cars && this.state.data.cars.length ? this.state.data.cars.map((dt, index) => (
                                        <label key={"car_" + dt.id} className={"btn btn-outline-primary text-left " + (index === 0 ? 'active' : '')}>
                                            <input type="radio" name="car_id"  autoComplete="off" value={dt.id} defaultChecked={this.state.car_id === dt.id ? true : false} onChange={this.handleChange} onClick={this.handleChange} /> {dt.car_name.toUpperCase()} <br/>{dt.make.toUpperCase() + ' - ' + dt.model.toUpperCase()} <br /><small>{dt.plate_number_en}</small>
                                        </label>    
                                                        )) :
                                    <h5 className="text-primary">{trans('front.no_cars_found_add_one')}</h5>
                                        }
                                    </div>
                                </div>
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button className="btn btn-primary btn-block rounded-pill" variant="secondary" disabled={this.state.data.cars && this.state.data.cars.length ? '' : 'disabled'} onClick={() => this.openModal('Booking')} >
                                {trans('front.book_parking_button').toUpperCase()}
                            </Button>
                        </Modal.Footer>                
                    </Modal>
                    <Modal 
                        show={this.state.isOpenBooking} 
                        onHide={() => this.closeModal('Booking')}
                        aria-labelledby="contained-modal-title-vcenter"
                        centered
                        >
                        <form onSubmit={this.bookParking}>
                            <Modal.Header className="bg-primary text-white" closeButton>
                                <Modal.Title>{trans('front.book_parking_button')}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div className="row align-items-center h-100">
                                    <div className="col-12">
                                        <div className="btn-group btn-group-vertical btn-group-toggle w-100" data-toggle="buttons">
                                            <label className="btn btn-outline-primary text-left active">
                                                <input type="radio" name="booking_type" id="immediate" autoComplete="off" value="1" defaultChecked={this.state.booking_type === "1" ? true : false} onChange={this.handleChange} onClick={this.handleChange} /> {trans('front.immediate_parking_button').toUpperCase()} <br /><small>{trans('front.immediate_parking_desc')}</small>
                                            </label>
                                            <label className="btn btn-outline-primary text-left">
                                                <input type="radio" name="booking_type" id="schedule" autoComplete="off" value="2" defaultChecked={this.state.booking_type === "2" ? true : false} onChange={this.handleChange} onClick={this.handleChange}/> {trans('front.schedule_parking_button').toUpperCase()} <br /><small>{trans('front.schedule_parking_desc')}</small>
                                            </label>
                                        </div>
                                    </div>
                                    <div className="col-12">
                                        <div className="card d-block w-100 mt-3">
                                            <div className="card-body">
                                                <div className="card-text">
                                                    <div className="row align-items-center h-100">
                                                        <div className="col-4 text-center">
                                                            {trans('front.park_for')}
                                                        </div>
                                                        <div className="col-4">
                                                            <InputNumber min={1} max={10} ref="duration" />
                                                        </div>
                                                        <div className="col-4 text-left">
                                                            {trans('front.hour_s_label')}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {this.state.booking_type === "2" ?
                                    <div className="col-12">
                                        <div className="card d-block w-100 mt-3">
                                            <div className="card-body">
                                                <div className="card-text">
                                                    <div className="row align-items-center h-100 text-center">
                                                        <div className="col-4">
                                                            {trans('front.from_label')}
                                                        </div>
                                                        <div className="col-8 text-center">
                                                            <DateTimePicker
                                                                onChange={this.handleDTPicker}
                                                                value={this.state.datetime}
                                                                minDate={new Date()}
                                                                required={true}
                                                                format='yyyy-MM-dd HH:mm:ss'
                                                                />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> : ''}
                                </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button className="btn btn-primary btn-block rounded-pill" variant="secondary" type="submit" disabled={this.state.loading ? 'disabled' : ''}>
                                    {trans('front.' + (this.state.booking_type === "1" ? 'immediate' : 'schedule') + '_parking_desc').toUpperCase()}
                                </Button>
                            </Modal.Footer>
                
                        </form>
                    </Modal>
                
                </div>
                )
    }
}
export default Home
