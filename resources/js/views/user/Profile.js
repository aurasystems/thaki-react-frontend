// AddCar.js

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ReactDOM from 'react-dom';
import FlashMessage from 'react-flash-message';
import {get_cookie, set_cookie, trans, unset_side_active, page_title, url} from '../../components/helpers/general_helper';
import {api_url, param} from '../../components/helpers/api_helper';

class Profile extends Component {
    constructor() {
        super();
        // Set page title
        page_title('front.profile_label');
        // Unset active sidebar
        unset_side_active();
        let state = {
            headers: {},
            data_object: {},
            formSubmitting: false,
            success_msg: false,
            error: '',
            errorMessage: [],
        };
        let AppState = get_cookie();
        if (AppState) {
            state.data_object = AppState.user;
            state.headers = {
                headers: {
                    'Authorization': AppState.user.token_type + ' ' + AppState.user.access_token,
                    [param('localization_header')]: document.documentElement.lang
                }
            }

        }
        this.state = state;
        // bind
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    // handle data change
    handleChange(el) {
        let inputName = el.target.id;
        let inputValue = el.target.type === 'checkbox' ? (el.target.checked ? 1 : 0) : el.target.value;
        let statusCopy = Object.assign({}, this.state);
        statusCopy.data_object[inputName] = inputValue;
        this.setState(statusCopy);
    }
    // create handleSubmit method right after handleChange method
    handleSubmit(e) {
        // stop browser's default behaviour of reloading on form submit
        e.preventDefault();
        this.setState({formSubmitting: true});
        ReactDOM.findDOMNode(this).scrollIntoView();
        axios.put(api_url(`update_profile`), this.state.data_object, this.state.headers)
                .then(response => {
                    if (response.data.success) {
                        this.setState({
                            success_msg: true,
                            formSubmitting: false
                        });
                        let responseData = response.data[param('data')];
                        // Update header name
                        $('#header_name').html(responseData[param('user')][param('name')]);
                        // Update local storage
                        let existing = get_cookie();

                        existing.user.name = responseData[param('user')][param('name')];
                        existing.user.email = responseData[param('user')][param('email')];
                        existing.user.phone = responseData[param('user')][param('phone')];
                        existing.user.age = responseData[param('user')][param('age')];
                        set_cookie(existing);

                    }
                }).
                catch(error => {
                    if (error.response) {
                        if (error.response.status === 401) {
                            this.props.history.push(url(param('logout_url')));
                        }
                        // The request was made and the server responded with a status code that falls out of the range of 2xx
                        let err = error.response.data;
                        this.setState({
                            error: err.message ? err.message.toString() : '',
                            errorMessage: err.errors ? err.errors : [],
                            formSubmitting: false
                        })
                    } else if (error.request) {
                        // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
                        let err = error.request;
                        this.setState({
                            error: err ? err.toString() : '',
                            formSubmitting: false
                        });
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        let err = error.message;
                        this.setState({
                            error: err ? err.toString() : '',
                            formSubmitting: false
                        })
                    }
                }).finally(this.setState({error: ''}));
    }
    render() {
        let errorMessage = this.state.errorMessage;
        let arr = [];
        Object.values(errorMessage).forEach((value) => (
                    arr.push(value)
                    ));
        return (
                <div className="card">
                    <div className="card-header bg-primary">{trans('front.profile_label')} <Link to={url('/dashboard')} className="btn btn-warning rounded-pill w-25 float-right" >{trans('front.cancel_button')}</Link></div>
                    <div className="card-body">
                        {this.state.success_msg ? <FlashMessage duration={900000} persistOnHover={true}>
                            <h5 className={"alert alert-success"}>{trans('front.saved_success_msg')}</h5></FlashMessage> : ''}
                        {this.state.error ? <FlashMessage duration={900000} persistOnHover={true}>
                            <h2 className={"alert alert-danger"}>{trans('front.error_label')}: {this.state.error}</h2>
                            <ul>
                                {arr.map((item, i) => (
                                                    <li key={i}><h5 style={{color: 'red'}}>{item}</h5></li>
                                                                    ))}
                            </ul></FlashMessage> : ''}
                        <form onSubmit={this.handleSubmit} autoComplete="off">
                            <div className="form-group">
                                <label htmlFor="name">{trans('front.name_field')}</label>
                                <input type="text" className="form-control" id="name" onChange={this.handleChange} value={this.state.data_object.name ? this.state.data_object.name : ''}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="email">{trans('front.email_field')}</label>
                                <input type="email" className="form-control" id="email" onChange={this.handleChange} value={this.state.data_object.email ? this.state.data_object.email : ''}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="phone">{trans('front.phone_field')}</label>
                                <input type="text" className="form-control" id="phone" onChange={this.handleChange} value={this.state.data_object.phone ? this.state.data_object.phone : ''}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="number">{trans('front.age_field')}</label>
                                <input type="text" className="form-control" id="age" onChange={this.handleChange} value={this.state.data_object.age ? this.state.data_object.age : ''}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">{trans('front.password_field')}</label>
                                <input type="password" className="form-control" id="password" onChange={this.handleChange} value={this.state.data_object.password ? this.state.data_object.password : ''}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="password_confirmation">{trans('front.password_confirmation_field')}</label>
                                <input type="password" className="form-control" id="password_confirmation" onChange={this.handleChange} value={this.state.data_object.password_confirmation ? this.state.data_object.password_confirmation : ''}/>
                            </div>
                            <div className="form-group text-center">
                                <button type="submit" className="btn btn-primary rounded-pill w-50" disabled={this.state.formSubmitting ? "disabled" : ""}>
                                    {trans('front.save_button')}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                );
    }
}

export default Profile;