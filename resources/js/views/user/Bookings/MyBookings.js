import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import FlashMessage from 'react-flash-message';
import {get_cookie, trans, side_active, page_title, flashMessage, url} from '../../../components/helpers/general_helper';
import {api_url, param} from '../../../components/helpers/api_helper';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import {Tabs, Tab, Modal, Button, Row, Nav, Col, Form, Card, Container} from "react-bootstrap";

// get our fontawesome imports
import { faTicketAlt, faQrcode, faTimesCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class MyBookings extends Component {
    constructor(props) {
        super(props);
        // Set page title
        page_title('front.my_parkings_label');
        // Set Sidebar active
        side_active('bookings');
        let state = {
            lang: document.documentElement.lang,
            isOpen: false,
            qr_path: null,
            loading: false,
            headers: {},
            error: '',
            errorMessage: [],
            data: {}
        };
        let AppState = get_cookie();
        if (AppState) {
            state.headers = {
                headers: {
                    'Authorization': AppState.user.token_type + ' ' + AppState.user.access_token,
                    [param('localization_header')]: document.documentElement.lang
                }
            };
        }
        this.state = state;
        // bind
        this.renderData = this.renderData.bind(this);
        this.getQrData = this.getQrData.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.cancelConfirm = this.cancelConfirm.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }
    // render bookings data by group
    renderData(type) {
        if (this.state.data[type]) {
            return this.state.data[type].map((item, index) => (
                        <div key={item.id} className="col-12 media">
                            <div className="card text-primary d-block w-100 mb-3">
                                <div className="card-body">
                                    <div className="card-text">
                                        <div className="row align-items-center h-100">
                                            <div className="col-2 text-center border-right">
                                                <FontAwesomeIcon icon={faTicketAlt} size="3x" />                        
                                            </div>
                                            <div className="col-8">
                                                <h4 className="card-title">{item.car.plate_number_en}</h4>
                                                <h5>{item.car.make + " - " + item.car.model}</h5>
                                                <div className="row">
                                                    <div className="col-6">
                                                        <h6><span dir="ltr">{item.date_from}</span></h6>
                                                        <h6><span dir="ltr">{item.time_from}</span></h6>
                                                    </div>
                                                    <div className="col-6">
                                                        <span dir="ltr">{item.date_to !== item.date_from ? item.date_to : ''}</span><br /><span dir="ltr">{item.time_to}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-2">
                                                {type === 'upcoming' ?
                                                    <div className="btn-group-vertical">
                                                        <button type="button" className="btn btn-primary btn-icon" title={trans('front.qr_button')}  onClick={() => this.getQrData(item.id)} disabled={this.state.loading ? "disabled" : ""}><FontAwesomeIcon icon={faQrcode} /></button>
                                                        <button type="button" className="btn btn-warning btn-icon" title={trans('front.cancel_button')} onClick={() => this.cancelConfirm(item.id)} disabled={this.state.loading ? "disabled" : ""}><FontAwesomeIcon icon={faTimesCircle} /></button>
                                                    </div>
                                                            : type === 'past' ?
                                                    <span className="badge badge-success">{trans('front.completed_label')}</span>
                                                            :
                                                    <span className="badge badge-danger">{trans('front.cancelled_label')}</span>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        ));
        } else
            return (
                    <div key={type} className="col-12 media mb-3">
                        <h5 className="text-primary">{trans('front.no_bookings_found')}</h5>
                    </div>
                    );
    }
    openModal() {
        this.setState({isOpen: true});
    }
    closeModal() {
        this.setState({
            isOpen: false,
            qr_path: null
        });
    }
// get all data from backend
    getData() {
        axios.get(api_url(`booking`), this.state.headers).
                then((response) => {
                    if (response.data.success) {
                        this.setState({data: response.data[param('data')].booking_list});
                    }
                }).
                catch(error => {
                    if (error.response) {
                        if (error.response.status === 401) {
                            this.props.history.push(url(param('logout_url')));
                        }
                        // The request was made and the server responded with a status code that falls out of the range of 2xx
                        let err = error.response.data;
                        this.setState({
                            error: err.message ? err.message.toString() : '',
                            errorMessage: err.errors ? err.errors : [],
                        })
                    } else if (error.request) {
                        console.log(error.request);
                        // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
                        let err = error.request;
                        this.setState({
                            error: err ? err.toString() : ''
                        });
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        let err = error.message;
                        this.setState({
                            error: err ? err.toString() : ''
                        });
                    }
                });
    }
// get all data from backend
    getQrData(id) {
        this.setState({loading: true});
        axios.post(api_url(`booking/qrcode`), {booking_id: id}, this.state.headers).
                then((response) => {
                    this.setState({loading: false});
                    if (response.data.success) {
                        this.setState({qr_path: response.data[param('data')].qr_path});
                        this.openModal();
                    }
                }).
                catch(error => {
                    this.setState({loading: false});
                    if (error.response) {
                        if (error.response.status === 401) {
                            this.props.history.push(url(param('logout_url')));
                        }
                        // The request was made and the server responded with a status code that falls out of the range of 2xx
                        let err = error.response.data;
                        this.setState({
                            error: err.message ? err.message.toString() : '',
                            errorMessage: err.errors ? err.errors : [],
                        })
                    } else if (error.request) {
                        // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
                        let err = error.request;
                        this.setState({
                            error: err ? err.toString() : ''
                        });
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        let err = error.message;
                        this.setState({
                            error: err ? err.toString() : ''
                        });
                    }
                });
    }
// lifecycle method
    componentDidMount() {
        this.getData();
    }
// Confirm cancel
    cancelConfirm(id) {
        confirmAlert({
            title: trans('front.confirm_action_msg'),
            message: trans('front.confirm_cancel_booking_msg'),
            buttons: [
                {
                    label: trans('front.confirm_button'),
                    onClick: () => this.handleCancel(id)
                },
                {
                    label: trans('front.cancel_button'),
                }
            ]
        });
    }
// Handle cancel
    handleCancel(id) {
        this.setState({loading: true});
        // make delete request to the backend
        axios.delete(api_url(`booking/${id}`), this.state.headers).
                then((response) => {
                    this.setState({loading: false});
                    if (response.data.success) {
                        // Refresh data list
                        this.getData();
                        // Display success message
                        flashMessage(response.data.message, 'success', 3000);
                    }
                }).
                catch(error => {
                    this.setState({loading: false});
                    if (error.response) {
                        if (error.response.status === 401) {
                            this.props.history.push(url(param('logout_url')));
                        }
                        // The request was made and the server responded with a status code that falls out of the range of 2xx
                        let err = error.response.data;
                        this.setState({
                            error: err.message ? err.message.toString() : '',
                            errorMessage: err.errors ? err.errors : [],
                        })
                    } else if (error.request) {
                        // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
                        let err = error.request;
                        this.setState({
                            error: err ? err.toString() : ''
                        });
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        let err = error.message;
                        this.setState({
                            error: err ? err.toString() : ''
                        });
                    }
                });
    }
// Display flash message
    flashMessage(phrase, type, duration) {
        // Remove flash message after duration
        setTimeout(() => {
            this.setState({flashMessage: ''});
        }, duration);
        // Return message element
        return (
                <FlashMessage duration={duration} persistOnHover={true}>
                    <h5 className={"alert alert-" + type}>{phrase}</h5>
                </FlashMessage>
                );
    }
    render() {
        let errorMessage = this.state.errorMessage;
        let arr = [];
        Object.values(errorMessage).forEach((value) => (
                    arr.push(value)
                    ));
        return (
                <div className="card">
                    <div className="card-header bg-primary">
                        {trans('front.my_parkings_label')}
                    </div>
                    <div className="card-body">
                        <div className="flash_message"></div>
                        {this.state.error ? <FlashMessage duration={900000} persistOnHover={true}>
                            <h2 className={"alert alert-danger"}>{trans('front.error_label')}: {this.state.error}</h2>
                            <ul>
                                {arr.map((item, i) => (
                                                    <li key={i}><h5 style={{color: 'red'}}>{item}</h5></li>
                                                                    ))}
                            </ul></FlashMessage> : ''}
                        <Row>
                            <Col>
                            <Tabs fill justify defaultActiveKey="upcoming" id="bookings">
                                <Tab eventKey="upcoming" title={trans('front.upcoming_label')}>
                                    <Row className="p-3">
                                        {this.renderData('upcoming')}
                                    </Row>
                                </Tab>
                                <Tab eventKey="completed" title={trans('front.completed_label')}>
                                    <Row className="p-3"> 
                                        {this.renderData('past')}
                                    </Row>
                                </Tab>
                                <Tab eventKey="cancelled" title={trans('front.cancelled_label')}>
                                    <Row className="p-3">
                                        {this.renderData('cancelled')}                
                                    </Row>
                                </Tab>
                            </Tabs>
                            </Col>
                        </Row>
                    </div>
                
                    <Modal 
                        show={this.state.isOpen} 
                        onHide={this.closeModal}
                        aria-labelledby="contained-modal-title-vcenter"
                        centered
                        >
                        <Modal.Header className="bg-primary text-white" closeButton>
                            <Modal.Title>{trans('front.qr_button')}</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <img src={this.state.qr_path} width="100%" /> 
                        </Modal.Body>
                        <Modal.Footer>
                            <Button className="btn-primary btn-block" variant="secondary" onClick={this.closeModal}>
                                {trans('front.close_button')}
                            </Button>
                        </Modal.Footer>
                    </Modal>
                </div>
                );
    }
}
export default MyBookings;