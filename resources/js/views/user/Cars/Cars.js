import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import FlashMessage from 'react-flash-message';
import {get_cookie, trans, side_active, page_title, flashMessage, url} from '../../../components/helpers/general_helper';
import {api_url, param} from '../../../components/helpers/api_helper';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
class Cars extends Component {
    constructor(props) {
        super(props);
        // Set page title
        page_title('front.cars_label');
        // Set Sidebar active
        side_active('cars');
        let state = {
            lang: document.documentElement.lang,
            headers: {},
            error: '',
            errorMessage: [],
            data: []
        };
        let AppState = get_cookie();
        if (AppState) {
            state.headers = {
                headers: {
                    'Authorization': AppState.user.token_type + ' ' + AppState.user.access_token,
                    [param('localization_header')]: document.documentElement.lang
                }
            };
        }
        this.state = state;
        // bind
        this.renderData = this.renderData.bind(this);
        this.deleteConfirm = this.deleteConfirm.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }
    // render data
    renderData() {
        return this.state.data && this.state.data.length ? this.state.data.map(dt => (
                    <div key={dt.id} className="col-12 media">
                        <div className="media-body">
                            <div>
                                {dt.car_name}{' '}{dt.is_default == 1 ?
                                            <span className="text-primary">{trans('front.car_default_label')}</span> : ''}
                                <span className="text-muted">
                                    {' '}
                                    <br />{this.state.lang == 'ar' ? dt.plate_number_ar : dt.plate_number_en}{' - ' + dt.make + ' ' + dt.model}
                                </span>
                                <div className="btn-group float-right">
                                    <Link className="btn btn-sm btn-primary" to={url(`/cars/edit/${dt.id}`)}>
                                    {trans('front.edit_button')}
                                    </Link>
                                    <button onClick={() => this.deleteConfirm(dt.id)} className="btn btn-sm btn-warning">
                                        {trans('front.delete_button')}
                                    </button>
                                </div>
                            </div>
                            <hr />
                        </div>
                    </div>
                    )) :
                (
                        <div className="col-12">
                            <h5 className="text-primary">{trans('front.no_cars_found_add_one')}</h5>
                        </div>
                        );
    }
    // get all data from backend
    getData() {
        axios.get(api_url(`cars`), this.state.headers).
                then((response) => {
                    if (response.data.success) {
                        console.log(response.data);
                        this.setState({data: [...response.data[param('data')].cars_list]})
                    }
                }).
                catch(error => {
                    if (error.response) {
                        if (error.response.status === 401) {
                            this.props.history.push(url(param('logout_url')));
                        }
                        // The request was made and the server responded with a status code that falls out of the range of 2xx
                        let err = error.response.data;
                        this.setState({
                            error: err.message ? err.message.toString() : '',
                            errorMessage: err.errors ? err.errors : [],
                        })
                    } else if (error.request) {
                        console.log(error.request);
                        // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
                        let err = error.request;
                        this.setState({
                            error: err ? err.toString() : ''
                        });
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        let err = error.message;
                        this.setState({
                            error: err ? err.toString() : ''
                        });
                    }
                });
    }
    // lifecycle method
    componentDidMount() {
        this.getData();
    }
    deleteConfirm(id) {
        confirmAlert({
            title: trans('front.confirm_action_msg'),
            message: trans('front.confirm_delete_msg'),
            buttons: [
                {
                    label: trans('front.confirm_button'),
                    onClick: () => this.handleDelete(id)
                },
                {
                    label: trans('front.cancel_button'),
                }
            ]
        });
    }
    // handle delete
    handleDelete(id) {
        // make delete request to the backend
        axios.delete(api_url(`cars/${id}`), this.state.headers).
                then((response) => {
                    if (response.data.success) {
                        // remove from local state
                        const isNotId = dt => dt.id !== id;
                        const updatedData = this.state.data.filter(isNotId);
                        this.setState({data: updatedData});
                        // Display success message
                        flashMessage(response.data.message, 'success', 3000);
                    }
                }).
                catch(error => {
                    if (error.response) {
                        if (error.response.status === 401) {
                            this.props.history.push(url(param('logout_url')));
                        }
                        // The request was made and the server responded with a status code that falls out of the range of 2xx
                        let err = error.response.data;
                        this.setState({
                            error: err.message ? err.message.toString() : '',
                            errorMessage: err.errors ? err.errors : [],
                        })
                    } else if (error.request) {
                        console.log(error.request);
                        // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
                        let err = error.request;
                        this.setState({
                            error: err ? err.toString() : ''
                        });
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        let err = error.message;
                        this.setState({
                            error: err ? err.toString() : ''
                        });
                    }
                });
    }
    // Display flash message
    flashMessage(phrase, type, duration) {
        // Remove flash message after duration
        setTimeout(() => {
            this.setState({flashMessage: ''});
        }, duration);
        // Return message element
        return (
                <FlashMessage duration={duration} persistOnHover={true}>
                    <h5 className={"alert alert-" + type}>{phrase}</h5>
                </FlashMessage>
                );
    }
    render() {
        let errorMessage = this.state.errorMessage;
        let arr = [];
        Object.values(errorMessage).forEach((value) => (
                    arr.push(value)
                    ));
        return (
                <div className="card">
                    <div className="card-header bg-primary">
                        {trans('front.cars_label')}
                        <Link to={url('/cars/add')} className="btn btn-warning rounded-pill w-25 float-right">
                        {trans('front.add_car_button')}
                        </Link>
                    </div>
                    <div className="card-body">
                        {this.state.error ? <FlashMessage duration={900000} persistOnHover={true}>
                            <h2 className={"alert alert-danger"}>{trans('front.error_label')}: {this.state.error}</h2>
                            <ul>
                                {arr.map((item, i) => (
                                                    <li key={i}><h5 style={{color: 'red'}}>{item}</h5></li>
                                                                    ))}
                            </ul></FlashMessage> : ''}
                        <div className="row">
                            {this.renderData()}
                        </div>
                    </div>
                </div>
                );
    }
}
export default Cars;