import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ReactDOM from 'react-dom';
import FlashMessage from 'react-flash-message';
import {get_cookie, trans, side_active, page_title, url} from '../../../components/helpers/general_helper';
import {api_url, param} from '../../../components/helpers/api_helper';

class EditCar extends Component {
    constructor(props) {
        super(props);
        // Set page title
        page_title('front.edit_car_label')
        // Set Sidebar active
        side_active('cars');
        let state = {
            lang: document.documentElement.lang,
            headers: {},
            states: [],
            data_object: {},
            formSubmitting: false,
            success_msg: false,
            error: '',
            errorMessage: [],
        };
        let AppState = get_cookie();
        if (AppState) {
            state.headers = {
                headers: {
                    'Authorization': AppState.user.token_type + ' ' + AppState.user.access_token,
                    [param('localization_header')]: document.documentElement.lang
                }
            };
        }
        this.state = state;
        // bind
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    // Get state list from backend
    getStates() {
        axios.get(api_url(`states`), this.state.headers).then(response => {
            return response;
        }).then(json => {
            if (json.data.success) {
                this.setState({states: json.data[param('data')].state_list});
            } else {
                this.props.history.push(url('/cars'));
            }
        }).catch(() => {
            this.props.history.push(url('/cars'));
        });
    }
    // handle data change
    handleChange(el) {
        let inputName = el.target.id;
        let inputValue = el.target.type === 'checkbox' ? (el.target.checked ? 1 : 0) : el.target.value;
        let statusCopy = Object.assign({}, this.state);
        statusCopy.data_object[inputName] = inputValue;
        this.setState(statusCopy);
    }
    // create handleSubmit method right after handleChange method
    handleSubmit(e) {
        // stop browser's default behaviour of reloading on form submit
        e.preventDefault();
        this.setState({formSubmitting: true});
        ReactDOM.findDOMNode(this).scrollIntoView();
        axios.put(api_url(`cars/${this.props.match.params.id}`), this.state.data_object, this.state.headers)
                .then(response => {
                    if (response.data.success) {
                        this.setState({
                            success_msg: true,
                            formSubmitting: false
                        });
                    }
                }).
                catch(error => {
                    if (error.response) {
                        if (error.response.status === 401) {
                            this.props.history.push(url(param('logout_url')));
                        }
                        // The request was made and the server responded with a status code that falls out of the range of 2xx
                        let err = error.response.data;
                        this.setState({
                            error: err.message ? err.message.toString() : '',
                            errorMessage: err.errors ? err.errors : [],
                            formSubmitting: false
                        })
                    } else if (error.request) {
                        // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
                        let err = error.request;
                        this.setState({
                            error: err ? err.toString() : '',
                            formSubmitting: false
                        });
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        let err = error.message;
                        this.setState({
                            error: err ? err.toString() : '',
                            formSubmitting: false
                        })
                    }
                }).finally(this.setState({error: ''}));
    }
    // get data from backend
    getData() {
        axios.get(api_url(`cars/${this.props.match.params.id}`), this.state.headers).then(response => {
            return response;
        }).then(json => {
            if (json.data.success) {
                this.setState({data_object: json.data[param('data')].car});
            } else {
                this.props.history.push(url('/cars'));
            }
        }).catch(() => {
            this.props.history.push(url('/cars'));
        });
    }
    // lifecycle method
    componentDidMount() {
        this.getStates();
        this.getData();
    }
    render() {
        let stateList = this.state.states;
        let sarr = [];
        Object.values(stateList).forEach((value) => (
                    sarr.push(value)
                    ));
        let errorMessage = this.state.errorMessage;
        let arr = [];
        Object.values(errorMessage).forEach((value) => (
                    arr.push(value)
                    ));
        return (
                <div className="card">
                    <div className="card-header bg-primary">{trans('front.edit_car_label') + (this.state.data_object.car_name ? ' (' + this.state.data_object.car_name + ')' : '')} <Link to={url('/cars')} className="btn btn-warning rounded-pill w-25 float-right" >{trans('front.cancel_button')}</Link></div>
                    <div className="card-body">
                        {this.state.success_msg ? <FlashMessage duration={9000000} persistOnHover={true}>
                            <h5 className={"alert alert-success"}>{trans('front.saved_success_msg')}</h5></FlashMessage> : ''}
                        {this.state.error ? <FlashMessage duration={900000} persistOnHover={true}>
                            <h2 className={"alert alert-danger"}>{trans('front.error_label')}: {this.state.error}</h2>
                            <ul>
                                {arr.map((item, i) => (
                                                    <li key={i}><h5 style={{color: 'red'}}>{item}</h5></li>
                                                                    ))}
                            </ul></FlashMessage> : ''}
                        <form onSubmit={this.handleSubmit}>
                            <div className="form-group">
                                <label htmlFor="car_name">{trans('front.car_name_field')}</label>
                                <input type="text" className="form-control" id="car_name" onChange={this.handleChange} value={this.state.data_object.car_name ? this.state.data_object.car_name : ''}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="plate_number_en">{trans('front.plate_number_en_field')}</label>
                                <input type="text" className="form-control" id="plate_number_en" onChange={this.handleChange} value={this.state.data_object.plate_number_en ? this.state.data_object.plate_number_en : ''}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="state">{trans('front.state_field')}</label>
                                <select className="form-control" name="state" id="state" onChange={this.handleChange} value={this.state.data_object.state ? this.state.data_object.state : ''} required>
                                    {
                                        sarr.map((s, index) => {
                                            return <option key={`state${index}`} value={s.id}>{s['name_' + this.state.lang]}</option>
                                        })
                                    }
                                </select>
                            </div>
                            <div className="form-group">
                                <label htmlFor="make">{trans('front.make_field')}</label>
                                <input type="text" className="form-control" id="make" onChange={this.handleChange} value={this.state.data_object.make ? this.state.data_object.make : ''}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="model">{trans('front.model_field')}</label>
                                <input type="text" className="form-control" id="model" onChange={this.handleChange} value={this.state.data_object.model ? this.state.data_object.model : ''}/>
                            </div>
                            <div className="form-group">
                                <div className="custom-control custom-switch">
                                    <input className="custom-control-input" type="checkbox" id="is_default" checked={this.state.data_object.is_default ? 'checked' : ''} onChange={this.handleChange} />
                                    <label className="custom-control-label" htmlFor="is_default" >{trans('front.car_default_field')}</label>
                                </div>
                            </div>
                            <div className="form-group text-center">
                                <button type="submit" className="btn btn-primary rounded-pill w-50" disabled={this.state.formSubmitting ? "disabled" : ""}>
                                    {trans('front.save_button')}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                );
    }
}

export default EditCar;