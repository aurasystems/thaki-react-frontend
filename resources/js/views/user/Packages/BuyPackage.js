import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import FlashMessage from 'react-flash-message';
import {get_cookie, trans, page_title, flashMessage, unset_side_active, url} from '../../../components/helpers/general_helper';
import {api_url, param} from '../../../components/helpers/api_helper';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
class BuyPackage extends Component {
    constructor(props) {
        super(props);
        // Set page title
        page_title('front.buy_package_label');
        // Unset active sidebar
        unset_side_active();
        let state = {
            lang: document.documentElement.lang,
            headers: {},
            error: '',
            errorMessage: [],
            data: []
        };
        let AppState = get_cookie();
        if (AppState) {
            state.headers = {
                headers: {
                    'Authorization': AppState.user.token_type + ' ' + AppState.user.access_token,
                    [param('localization_header')]: document.documentElement.lang
                }
            };
        }
        this.state = state;
        // bind
        this.renderData = this.renderData.bind(this);
        this.actionConfirm = this.actionConfirm.bind(this);
        this.handleAction = this.handleAction.bind(this);
    }
    // render data
    renderData() {
        return this.state.data.map((dt, index) => (
                    <div key={dt.id} className="col-6 media">
                        <div className={(index === 1 || Number.isInteger(((index + 1) / 3)) ? "bg-primary" : "") + " card d-block w-100 mb-3"}>
                            <div className="card-body">
                                <div className="card-text">
                                    <div className="row align-items-center h-100">
                                        <div className="col-12 p-4">
                                            <h5 className={(index === 1 || Number.isInteger(((index + 1) / 3)) ? "text-white" : "text-primary") + " text-center"}>{dt.name.toUpperCase()} ({dt.amount_of_hours + " " + trans('front.hour_plural')})</h5>
                                        </div>
                                        <div className="col-12">
                                            <span className="float-left">{trans('front.validity_label') + ": " + dt.expiry_after + " " + trans('front.day_plural')}</span>
                                            <span className="float-right">{dt.price + " " + trans('front.currency_label')}</span>
                                        </div>
                                        <div className="col-12">
                                            <hr />
                                            <div className="btn-group float-right">
                                                <button onClick={() => this.actionConfirm(dt.id)} className="btn btn-sm btn-warning">
                                                    {trans('front.select_button')}
                                                </button>
                                            </div>        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    ));
    }
    // get all data from backend
    getData() {
        axios.get(api_url(`packages`), this.state.headers).
                then((response) => {
                    if (response.data.success) {
                        console.log(response.data);
                        this.setState({data: [...response.data[param('data')].packages_list]})
                    }
                }).
                catch(error => {
                    if (error.response) {
                        if (error.response.status === 401) {
                            this.props.history.push(url(param('logout_url')));
                        }
                        // The request was made and the server responded with a status code that falls out of the range of 2xx
                        let err = error.response.data;
                        this.setState({
                            error: err.message ? err.message.toString() : '',
                            errorMessage: err.errors ? err.errors : [],
                        })
                    } else if (error.request) {
                        console.log(error.request);
                        // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
                        let err = error.request;
                        this.setState({
                            error: err ? err.toString() : ''
                        });
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        let err = error.message;
                        this.setState({
                            error: err ? err.toString() : ''
                        });
                    }
                });
    }
    // lifecycle method
    componentDidMount() {
        this.getData();
    }
    // Confirm action
    actionConfirm(id) {
        confirmAlert({
            title: trans('front.confirm_action_msg'),
            message: trans('front.confirm_buy_package_msg'),
            buttons: [
                {
                    label: trans('front.confirm_button'),
                    onClick: () => this.handleAction(id)
                },
                {
                    label: trans('front.cancel_button'),
                }
            ]
        });
    }
    // handle action
    handleAction(id) {
        // make purchase request to the backend
        axios.post(api_url(`packages/buy`), {package_id: id}, this.state.headers).
                then((response) => {
                    if (response.data.success) {
                        // Display success message
                        flashMessage(response.data.message, 'success', 2000);
                        setTimeout(() => {
                            this.props.history.push(url('/dashboard'));
                        }, 2000);
                    }
                }).
                catch(error => {
                    if (error.response) {
                        if (error.response.status === 401) {
                            this.props.history.push(url(param('logout_url')));
                        }
                        // The request was made and the server responded with a status code that falls out of the range of 2xx
                        let err = error.response.data;
                        this.setState({
                            error: err.message ? err.message.toString() : '',
                            errorMessage: err.errors ? err.errors : [],
                        })
                    } else if (error.request) {
                        console.log(error.request);
                        // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
                        let err = error.request;
                        this.setState({
                            error: err ? err.toString() : ''
                        });
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        let err = error.message;
                        this.setState({
                            error: err ? err.toString() : ''
                        });
                    }
                });
    }

    // Display flash message
    flashMessage(phrase, type, duration) {
        // Remove flash message after duration
        setTimeout(() => {
            this.setState({flashMessage: ''});
        }, duration);
        // Return message element
        return (
                <FlashMessage duration={duration} persistOnHover={true}>
                    <h5 className={"alert alert-" + type}>{phrase}</h5>
                </FlashMessage>
                );
    }
    render() {
        let errorMessage = this.state.errorMessage;
        let arr = [];
        Object.values(errorMessage).forEach((value) => (
                    arr.push(value)
                    ));
        return (
                <div className="card">
                    <div className="card-header bg-primary">
                        {trans('front.select_package_label')}
                        <Link to={url('/dashboard')} className="btn btn-warning rounded-pill w-25 float-right">
                        {trans('front.cancel_button')}
                        </Link>
                    </div>
                    <div className="card-body">
                        <div className="flash_message"></div>
                        {this.state.error ? <FlashMessage duration={900000} persistOnHover={true}>
                            <h2 className={"alert alert-danger"}>{trans('front.error_label')}: {this.state.error}</h2>
                            <ul>
                                {arr.map((item, i) => (
                                                    <li key={i}><h5 style={{color: 'red'}}>{item}</h5></li>
                                                                    ))}
                            </ul></FlashMessage> : ''}
                        <div className="row">
                            {this.renderData()}
                        </div>
                    </div>
                </div>
                );
    }
}
export default BuyPackage;