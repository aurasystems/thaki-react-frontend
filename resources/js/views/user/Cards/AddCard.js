import React, { Component }
from 'react';
import { Link }
from 'react-router-dom';
import ReactDOM from 'react-dom';
import FlashMessage from 'react-flash-message';
import validator from 'validator';
import Cards from 'react-credit-cards';
import 'react-credit-cards/es/styles-compiled.css';
import {get_cookie, trans, side_active, page_title, years, months, url} from '../../../components/helpers/general_helper';
import {api_url, param} from '../../../components/helpers/api_helper';

class AddCard extends Component {

    constructor() {
        super();
        // Set page title   
        page_title('front.add_card_label')
        // Set Sidebar active
        side_active('cards');
        // assign state
        let state = {
            headers: {},
            data_object: {
                card_holder: '',
                card_number: '',
                card_expiry: '',
                card_cvv: '',
            },
            months: [],
            expiry_month: '',
            expiry_year: '',
            expiry: '',
            formSubmitting: false,
            success_msg: false,
            setErrorMessage: '',
            error: '',
            errorMessage: [],
            focus: ''
        };
        let AppState = get_cookie();
        if (AppState) {
            // set auth header
            state.headers = {
                headers: {
                    'Authorization': AppState.user.token_type + ' ' + AppState.user.access_token,
                    [param('localization_header')]: document.documentElement.lang
                }
            }

        }
        this.state = state;
        // assign years array
        this.years = years();
        // assign card expiry related inputs id
        this.expiryFields = ['expiry_month', 'expiry_year'];
        // bind
        this.handleInputFocus = this.handleInputFocus.bind(this);
        this.handleInputKeyDown = this.handleInputKeyDown.bind(this);
        this.populateMonths = this.populateMonths.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.validateCreditCard = this.validateCreditCard.bind(this);
        this.handleExpiry = this.handleExpiry.bind(this);
        this.validateCreditCVV = this.validateCreditCVV.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    // handle input key down
    handleInputKeyDown(e) {
        this.setState({focus: ''});
    }
    // handle input focus
    handleInputFocus(e) {
        let inputName = e.target.id;
        let inputValue = this.expiryFields.includes(inputName) ? 'expiry' : e.target.name;
        this.setState({focus: inputValue});
    }
    // handle card holder change
    handleChange(el) {
        let inputName = el.target.id;
        let inputValue = el.target.value;
        const re = /^[a-zA-Z ]+$/; //rules
        if (el.target.value === "" || re.test(el.target.value)) {
            let statusCopy = Object.assign({}, this.state);
            statusCopy.data_object[inputName] = inputValue;
            this.setState(statusCopy);
        }
    }
    // handle credit card number validation
    validateCreditCard(el) {
        let inputName = el.target.id;
        let inputValue = el.target.type === 'checkbox' ? (el.target.checked ? 1 : 0) : el.target.value;
        let statusCopy = Object.assign({}, this.state);
        let val = parseInt(inputValue);
        statusCopy.data_object[inputName] = val ? val : '';
        this.setState(statusCopy);
        if (el.target.value.length === 16) {
            if (!validator.isCreditCard(el.target.value)) {
                statusCopy.data_object[inputName] = '';
                this.setState(statusCopy);
            }
        }
    }
    // Populate month upon select year
    populateMonths(e) {
        this.state.months = months(e.target.value);
    }
    // handle credit expiry date
    handleExpiry(e) {
        let inputName = e.target.id;
        let inputValue = e.target.value;
        let state = [];
        if (inputName === 'expiry_year' && inputValue) {
            this.populateMonths(e);
            state['expiry_month'] = '';
        }
        state[inputName] = inputValue;
        state['focus'] = 'expiry';
        this.setState(state);
        setTimeout(() => {
            if (this.state.expiry_month || this.state.expiry_year) {
                let statusCopy = Object.assign({}, this.state);
                if (this.state.expiry_month && this.state.expiry_year) {
                    statusCopy.data_object['card_expiry'] = this.state.expiry_year + '-' + this.state.expiry_month;
                }
                statusCopy['expiry'] = this.state.expiry_month + '' + this.state.expiry_year.substr(2, 2);
                this.setState(statusCopy);
            }
        }, 50);
    }
    // handle credit cvv validation
    validateCreditCVV(el) {
        let inputName = el.target.id;
        let inputValue = el.target.value;
        let statusCopy = Object.assign({}, this.state);
        let val = parseInt(inputValue);
        statusCopy.data_object[inputName] = val ? val : '';
        this.setState(statusCopy);
    }

    // create handleSubmit method right after handleChange method
    handleSubmit(e) {
        // stop browser's default behaviour of reloading on form submit
        e.preventDefault();
        this.setState({formSubmitting: true, focus: ''});
        ReactDOM.findDOMNode(this).scrollIntoView();
        axios.post(api_url(`cards`), this.state.data_object, this.state.headers)
                .then(response => {
                    if (response.data.success) {
                        this.setState({
                            success_msg: true,
                        });
                        setTimeout(() => {
                            this.props.history.push(url('/cards'));
                        }, 1000);
                    }
                }).
                catch(error => {
                    if (error.response) {
                        if (error.response.status === 401) {
                            this.props.history.push(url(param('logout_url')));
                        }
                        // The request was made and the server responded with a status code that falls out of the range of 2xx
                        let err = error.response.data;
                        this.setState({
                            error: err.message ? err.message.toString() : '',
                            errorMessage: err.errors ? err.errors : [],
                            formSubmitting: false
                        })
                    } else if (error.request) {
                        // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
                        let err = error.request;
                        this.setState({
                            error: err ? err.toString() : '',
                            formSubmitting: false
                        });
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        let err = error.message;
                        this.setState({
                            error: err ? err.toString() : '',
                            formSubmitting: false
                        });
                    }
                }).finally(this.setState({error: ''}));
    }
    render() {
        let monthList = this.state.months;
        let marr = [];
        Object.values(monthList).forEach((value) => (
                    marr.push(value)
                    ));
        let errorMessage = this.state.errorMessage;
        let arr = [];
        Object.values(errorMessage).forEach((value) => (
                    arr.push(value)
                    ));
        return (
                <div className="card">    
                    <div className="card-header bg-primary">{trans('front.add_card_label')} <Link to={url('/cards')} className="btn btn-warning rounded-pill w-25 float-right" >{trans('front.cancel_button')}</Link></div>
                
                    <div className="card-body">
                
                        {this.state.success_msg ? <FlashMessage duration={900000} persistOnHover={true}>
                            <h5 className={"alert alert-success"}>{trans('front.saved_success_msg')}</h5></FlashMessage> : ''}
                        {this.state.error ? <FlashMessage duration={900000} persistOnHover={true}>
                            <h2 className={"alert alert-danger"}>{trans('front.error_label')}: {this.state.error}</h2>
                            <ul>
                                {arr.map((item, i) => (
                                                    <li key={i}><h5 style={{color: 'red'}}>{item}</h5></li>
                                                                    ))}
                            </ul></FlashMessage> : ''}
                
                        <div className="row align-items-center h-100">
                            <div className="col-sm-5">
                                <Cards
                                    cvc={this.state.data_object.card_cvv}
                                    expiry={this.state.expiry}
                                    focused={this.state.focus}
                                    name={this.state.data_object.card_holder}
                                    number={this.state.data_object.card_number}
                                    preview={true}
                                    />
                            </div>
                            <div className="col-sm-7">
                                <form onSubmit={this.handleSubmit}>
                                    <div className="row">
                                        <div className="col-sm-12">
                                            <div className="form-group">
                                                <label htmlFor="card_holder">{trans('front.card_holder_field')}</label>
                                                <input className="form-control" id="card_holder" name="name" type="text" pattern="^[A-Za-z ]+$" maxLength="20" onFocus={this.handleInputFocus} onBlur={this.handleInputKeyDown} onChange={this.handleChange} value={this.state.data_object.card_holder} required/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-12">
                                            <div className="form-group">
                                                <label htmlFor="card_number">{trans('front.card_number_field')}</label>
                                                <input className="form-control" id="card_number" name="number" type="text" pattern="[0-9]*" inputMode="numeric" maxLength="16" onFocus={this.handleInputFocus} onBlur={this.handleInputKeyDown} onChange={this.validateCreditCard} placeholder="0000 0000 0000 0000" value={this.state.data_object.card_number} required/>
                
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-8">
                                            <div className="form-group">
                                                <label><span className="hidden-xs">{trans('front.card_expiry_field')}</span> </label>
                                                <div className="form-inline">
                                                    <select className="form-control" style={{width: 45 + '%'}} id="expiry_month" onFocus={this.handleInputFocus} onBlur={this.handleInputKeyDown} onChange={this.handleExpiry} value={this.state.expiry_month} required>
                                                        <option>MM</option>
                                                        {
                                                            marr.map((month, index) => {
                                                                return <option key={`month${index}`} value={month.pfx + '' + month.m}>{month.m + ' - ' + month.n}</option>
                                                            })
                                                        }
                                                    </select>
                                                    <span style={{width: 10 + '%', textAlign: 'center'}}> / </span>
                                                    <select className="form-control" style={{width: 45 + '%'}} id="expiry_year" onFocus={this.handleInputFocus} onBlur={this.handleInputKeyDown} onChange={this.handleExpiry} required>
                                                        <option value="">YY</option>
                                                        {
                                                            this.years.map((year, index) => {
                                                                return <option key={`year${index}`} value={year}>{year}</option>
                                                            })
                                                        }
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-4">
                                            <div className="form-group">
                                                <label htmlFor="card_cvv">{trans('front.card_cvv_field')}</label>
                                                <input className="form-control" id="card_cvv" name="cvc" type="text" pattern="[0-9]*" inputMode="numeric" maxLength="4" onFocus={this.handleInputFocus} 
                                                       onBlur={this.handleInputKeyDown} onChange={this.validateCreditCVV} placeholder={trans('front.card_cvv_ph')} value={this.state.data_object.card_cvv} required/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-12">
                                            <div className="form-group text-center">
                                                <button type="submit" className="btn btn-primary btn-block rounded-pill" disabled={this.state.formSubmitting ? "disabled" : ""}>
                                                    {trans('front.save_button')}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                
                            </div>
                        </div>
                    </div>
                </div>


                );
    }
}

export default AddCard;