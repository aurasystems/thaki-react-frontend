import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import FlashMessage from 'react-flash-message';
import {get_cookie, trans, side_active, page_title, flashMessage, url} from '../../../components/helpers/general_helper';
import {api_url, param} from '../../../components/helpers/api_helper';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
class Cards extends Component {

    constructor(props) {
        super(props);
        // Set page title
        page_title('front.cards_label');
        // Set Sidebar active
        side_active('cards');
        let state = {
            headers: {},
            error: '',
            errorMessage: [],
            data: []
        };
        let AppState = get_cookie();
        if (AppState) {
            state.headers = {
                headers: {
                    'Authorization': AppState.user.token_type + ' ' + AppState.user.access_token,
                    [param('localization_header')]: document.documentElement.lang
                }
            };
        }
        this.state = state;
        // bind
        this.renderData = this.renderData.bind(this);
        this.defaultConfirm = this.defaultConfirm.bind(this);
        this.handleDefault = this.handleDefault.bind(this);
        this.updateItems = this.updateItems.bind(this);
        this.deleteConfirm = this.deleteConfirm.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }
    // render data
    renderData() {
        return this.state.data && this.state.data.length ? this.state.data.map(dt => (
                    <div key={dt.id} className="col-6">
                        <div className={(dt.is_default ? "bg-primary" : "") + " card d-block w-100 mb-3"}>
                            <div className="card-body">
                                <div className="card-text">
                                    <div className="row align-items-center h-100">
                                        <div className="col-12">
                                            <h5 className={(dt.is_default ? "text-white" : "text-primary") + " text-center p-4"}>{dt.card_number}</h5>
                                        </div>
                                        <div className="col-12">
                                            <span className="float-right" dir="ltr">{dt.card_expiry}</span>
                                        </div>
                                        <div className="col-12">
                                            <hr />
                                            <div className="btn-group float-right">
                                                {dt.is_default === 0 ?
                                            <button className="btn btn-sm btn-primary" onClick={() => this.defaultConfirm(dt.id)}>
                                                {trans('front.set_default_button')}
                                            </button> : ''}
                                                <button onClick={() => this.deleteConfirm(dt.id)} className="btn btn-sm btn-warning">
                                                    {trans('front.delete_button')}
                                                </button>
                                            </div>        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    )) :
                (
                        <div className="col-12">
                            <h5 className="text-primary">{trans('front.no_cards_found_add_one')}</h5>
                        </div>
                        );
    }
    // get all data from backend
    getData() {
        axios.get(api_url(`cards`), this.state.headers).
                then((response) => {
                    if (response.data.success) {
                        this.setState({data: [...response.data[param('data')].card_list]})
                    }
                }).
                catch(error => {
                    if (error.response) {
                        if (error.response.status === 401) {
                            this.props.history.push(url(param('logout_url')));
                        }
                        // The request was made and the server responded with a status code that falls out of the range of 2xx
                        let err = error.response.data;
                        this.setState({
                            error: err.message ? err.message.toString() : '',
                            errorMessage: err.errors ? err.errors : [],
                        })
                    } else if (error.request) {
                        // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
                        let err = error.request;
                        this.setState({
                            error: err ? err.toString() : ''
                        });
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        let err = error.message;
                        this.setState({
                            error: err ? err.toString() : ''
                        });
                    }
                });
    }
    // lifecycle method
    componentDidMount() {
        this.getData();
    }
    defaultConfirm(id) {
        confirmAlert({
            title: trans('front.confirm_action_msg'),
            message: trans('front.confirm_default_msg'),
            buttons: [
                {
                    label: trans('front.confirm_button'),
                    onClick: () => this.handleDefault(id)
                },
                {
                    label: trans('front.cancel_button'),
                }
            ]
        });
    }
    // handle default
    handleDefault(id) {
        // make put request to the backend
        axios.put(api_url(`cards/${id}`), {is_default: 1}, this.state.headers).
                then((response) => {
                    if (response.data.success) {
                        // set item default
                        this.updateItems(id);
                        // Display success message
                        flashMessage(response.data.message, 'success', 3000);
                    }
                }).
                catch(error => {
                    if (error.response) {
                        if (error.response.status === 401) {
                            this.props.history.push(url(param('logout_url')));
                        }
                        // The request was made and the server responded with a status code that falls out of the range of 2xx
                        let err = error.response.data;
                        this.setState({
                            error: err.message ? err.message.toString() : '',
                            errorMessage: err.errors ? err.errors : [],
                        })
                    } else if (error.request) {
                        console.log(error.request);
                        // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
                        let err = error.request;
                        this.setState({
                            error: err ? err.toString() : ''
                        });
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        let err = error.message;
                        this.setState({
                            error: err ? err.toString() : ''
                        });
                    }
                });
    }
    updateItems(id) {
        // Find default items & unset default
        let dindex = this.state.data.findIndex(dt => dt.is_default === 1);
        if (dindex !== -1)
            this.setState({
                data: [
                    ...this.state.data.slice(0, dindex),
                    Object.assign({}, this.state.data[dindex], {is_default: 0}),
                    ...this.state.data.slice(dindex + 1)
                ]
            });
        // Find item index and set default
        let index = this.state.data.findIndex(dt => dt.id === id);
        if (index !== -1)
            this.setState({
                data: [
                    ...this.state.data.slice(0, index),
                    Object.assign({}, this.state.data[index], {is_default: 1}),
                    ...this.state.data.slice(index + 1)
                ]
            });

    }
    deleteConfirm(id) {
        confirmAlert({
            title: trans('front.confirm_action_msg'),
            message: trans('front.confirm_delete_msg'),
            buttons: [
                {
                    label: trans('front.confirm_button'),
                    onClick: () => this.handleDelete(id)
                },
                {
                    label: trans('front.cancel_button'),
                }
            ]
        });
    }
    // handle delete
    handleDelete(id) {
        // make delete request to the backend
        axios.delete(api_url(`cards/${id}`), this.state.headers).
                then((response) => {
                    if (response.data.success) {
                        // remove from local state
                        const isNotId = dt => dt.id !== id;
                        const updatedData = this.state.data.filter(isNotId);
                        this.setState({data: updatedData});
                        // Display success message
                        flashMessage(response.data.message, 'success', 3000);
                    }
                }).
                catch(error => {
                    if (error.response) {
                        if (error.response.status === 401) {
                            this.props.history.push(url(param('logout_url')));
                        }
                        // The request was made and the server responded with a status code that falls out of the range of 2xx
                        let err = error.response.data;
                        this.setState({
                            error: err.message ? err.message.toString() : '',
                            errorMessage: err.errors ? err.errors : [],
                        })
                    } else if (error.request) {
                        console.log(error.request);
                        // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
                        let err = error.request;
                        this.setState({
                            error: err ? err.toString() : ''
                        });
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        let err = error.message;
                        this.setState({
                            error: err ? err.toString() : ''
                        });
                    }
                });
    }
    render() {
        let errorMessage = this.state.errorMessage;
        let arr = [];
        Object.values(errorMessage).forEach((value) => (
                    arr.push(value)
                    ));
        return (
                <div className="card">
                    <div className="card-header bg-primary">
                        {trans('front.cards_label')}
                        <Link to={url('/cards/add')} className="btn btn-warning rounded-pill w-25 float-right">
                        {trans('front.add_card_button')}
                        </Link>
                    </div>
                    <div className="card-body">
                        <div className="flash_message"></div>
                        {this.state.error ? <FlashMessage duration={900000} persistOnHover={true}>
                            <h2 className={"alert alert-danger"}>{trans('front.error_label')}: {this.state.error}</h2>
                            <ul>
                                {arr.map((item, i) => (
                                                    <li key={i}><h5 style={{color: 'red'}}>{item}</h5></li>
                                                                    ))}
                            </ul></FlashMessage> : ''}
                        <div className="row">
                            {this.renderData()}
                        </div>
                    </div>
                </div>
                );
    }
}
export default Cards;