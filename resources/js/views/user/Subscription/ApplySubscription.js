import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ReactDOM from 'react-dom';
import FlashMessage from 'react-flash-message';
import {get_cookie, trans, unset_side_active, page_title, url} from '../../../components/helpers/general_helper';
import {api_url, param} from '../../../components/helpers/api_helper';

class ApplySubscription extends Component {
    constructor() {
        super();
        // Set page title
        page_title('front.apply_subscription_button')
        // Set Sidebar active
        unset_side_active('cars');
        let state = {
            headers: {},
            data_object: {},
            is_approved: 0,
            id_card_front_img: null,
            id_card_back_img: null,
            formSubmitting: false,
            success_msg: false,
            error: '',
            errorMessage: [],
        };
        let AppState = get_cookie();
        if (AppState) {
            state.is_approved = AppState.user.is_approved;
            state.data_object = {
                client_full_name: AppState.user.name,
                client_email: AppState.user.email,
                client_phones: AppState.user.phone
            }
            state.headers = {
                headers: {
                    'Authorization': AppState.user.token_type + ' ' + AppState.user.access_token,
                    [param('localization_header')]: document.documentElement.lang
                }
            }

        }
        this.state = state;
        // bind
        this.handleChange = this.handleChange.bind(this);
        this.onFileChange = this.onFileChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    componentDidMount() {
        if (this.state.is_approved) {
            this.props.history.push(url('/dashboard'));
        }
    }
    // handle data change
    handleChange(el) {
        let inputName = el.target.id;
        let inputValue = el.target.type === 'checkbox' ? (el.target.checked ? 1 : 0) : el.target.value;
        let statusCopy = Object.assign({}, this.state);
        statusCopy.data_object[inputName] = inputValue;
        this.setState(statusCopy);
    }
    // On file select (from the pop up)
    onFileChange(ev) {
        let inputName = ev.target.id;
        // Update the state
        let statusCopy = Object.assign({}, this.state);
        statusCopy.data_object[inputName] = ev.target.files[0];
        this.setState({[inputName]: ev.target.files[0]});

    }
    // create handleSubmit method right after handleChange method
    handleSubmit(e) {
        // stop browser's default behaviour of reloading on form submit
        e.preventDefault();
        this.setState({formSubmitting: true});
        ReactDOM.findDOMNode(this).scrollIntoView();
        const formData = new FormData();

        formData.append('client_full_name', this.state.data_object.client_full_name);
        formData.append('client_email', this.state.data_object.client_email);
        formData.append('client_phones', this.state.data_object.client_phones);
        formData.append('id_card_front_img', this.state.id_card_front_img);
        formData.append('id_card_back_img', this.state.id_card_back_img);

        axios.post(api_url(`subscriptions/request`), formData, this.state.headers)
                .then(response => {
                    if (response.data.success) {
                        this.setState({
                            success_msg: true,
                        });
                        setTimeout(() => {
                            this.props.history.push(url('/dashboard'));
                        }, 2000);
                    }
                }).
                catch(error => {
                    if (error.response) {
                        if (error.response.status === 401) {
                            this.props.history.push(url(param('logout_url')));
                        }
                        // The request was made and the server responded with a status code that falls out of the range of 2xx
                        let err = error.response.data;
                        this.setState({
                            error: err.message ? err.message.toString() : '',
                            errorMessage: err.errors ? err.errors : [],
                            formSubmitting: false
                        })
                    } else if (error.request) {
                        // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
                        let err = error.request;
                        this.setState({
                            error: err ? err.toString() : '',
                            formSubmitting: false
                        });
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        let err = error.message;
                        this.setState({
                            error: err ? err.toString() : '',
                            formSubmitting: false
                        })
                    }
                }).finally(this.setState({error: ''}));
    }
    render() {
        let errorMessage = this.state.errorMessage;
        let arr = [];
        Object.values(errorMessage).forEach((value) => (
                    arr.push(value)
                    ));
        return (
                <div className="card">
                    <div className="card-header bg-primary">{trans('front.apply_subscription_button')} <Link to={url('/dashboard')} className="btn btn-warning rounded-pill w-25 float-right" >{trans('front.cancel_button')}</Link></div>
                    <div className="card-body">
                        {this.state.success_msg ? <FlashMessage duration={900000} persistOnHover={true}>
                            <h5 className={"alert alert-success"}>{trans('front.saved_success_msg')}</h5></FlashMessage> : ''}
                        {this.state.error ? <FlashMessage duration={900000} persistOnHover={true}>
                            <h2 className={"alert alert-danger"}>{trans('front.error_label')}: {this.state.error}</h2>
                            <ul>
                                {arr.map((item, i) => (
                                                    <li key={i}><h5 style={{color: 'red'}}>{item}</h5></li>
                                                                    ))}
                            </ul></FlashMessage> : ''}
                        <form onSubmit={this.handleSubmit}>
                            <div className="form-group">
                                <label htmlFor="client_full_name">{trans('front.name_field')}</label>
                                <input type="text" className="form-control" id="client_full_name" onChange={this.handleChange} value={this.state.data_object.client_full_name ? this.state.data_object.client_full_name : ''} required/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="client_email">{trans('front.email_field')}</label>
                                <input type="email" className="form-control" id="client_email" onChange={this.handleChange} value={this.state.data_object.client_email ? this.state.data_object.client_email : ''} required/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="client_phones">{trans('front.phone_field')}</label>
                                <input type="text" className="form-control" id="client_phones" onChange={this.handleChange} value={this.state.data_object.client_phones ? this.state.data_object.client_phones : ''} required/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="id_card_front_img_field">{trans('front.id_card_front_img_field')}</label>
                                <input type="file" className="form-control" id="id_card_front_img" onChange={this.onFileChange} required/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="id_card_back_img">{trans('front.id_card_back_img_field')}</label>
                                <input type="file" className="form-control" id="id_card_back_img" onChange={this.onFileChange} required/>
                            </div>
                            <div className="form-group text-center">
                                <button type="submit" className="btn btn-primary rounded-pill w-50" disabled={this.state.formSubmitting ? "disabled" : ""}>
                                    {trans('front.submit_request_button')}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                );
    }
}

export default ApplySubscription;