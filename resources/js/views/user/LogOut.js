import React, { Component } from 'react';
import {get_cookie, set_cookie, url} from '../../components/helpers/general_helper';
import {api_url, param} from '../../components/helpers/api_helper';

class LogOut extends Component {
    constructor() {
        super();
        let state = {
            isLoggedIn: false,
            headers: {}
        };
        let AppState = get_cookie();
        if (AppState) {
            state.isLoggedIn = AppState.isLoggedIn;
            state.headers = {
                headers: {
                    'Authorization': AppState.user.token_type + ' ' + AppState.user.access_token,
                    [param('localization_header')]: document.documentElement.lang
                }
            };
        }
        this.state = state;
    }
    // lifecycle method
    componentDidMount() {
        if (!this.state.isLoggedIn) {
            return this.props.history.push(url('/'));
        }
        this.logOut();
    }
    logOut() {
        axios.post(api_url(`auth/logout`), {}, this.state.headers);
        let appState = {
            isLoggedIn: false,
            user: {}
        };
        set_cookie(appState);
        this.setState(appState);
        location.reload()
    }
    render() {
        return null
    }
}
export default LogOut;