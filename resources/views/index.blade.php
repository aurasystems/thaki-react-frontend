<!doctype html>
<html lang="{{ __('front.lang_code') }}" dir="{{ __('front.lang_dir') }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <!-- React Localization -->
        <script>window.translations = @json(session() -> get('translations'));</script>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <!-- Styles -->
        @if ( __('front.lang_dir') == 'rtl' )
        <link 
            rel="stylesheet" 
            href="//cdn.rtlcss.com/bootstrap/v4.5.3/css/bootstrap.min.css" 
            integrity="sha384-JvExCACAZcHNJEc7156QaHXTnQL3hQBixvj5RV5buE7vgnNEzzskDtx9NQ4p6BJe" 
            crossorigin="anonymous">
        @else
        <link 
            rel="stylesheet" 
            href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" 
            integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" 
            crossorigin="anonymous">
        @endif

        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    </head>
    @if ( __('front.lang_dir') == 'rtl' )
    <body class="rtl">
        @else
    <body>
        @endif
        <div id="app">

        </div>
    </body>
</html>
