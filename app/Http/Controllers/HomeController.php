<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller {

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request) {
        $lang = $request->locale;
        if (array_key_exists($lang, Config::get('languages'))) {
            Session::put('applocale', $lang);
            $url = $request->path;
            return redirect('/' . ($url ? $url : ''));
        }
        return view('index');
    }

}
